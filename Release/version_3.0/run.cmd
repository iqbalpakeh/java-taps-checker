@ECHO OFF

REM ------------------------------------------------------------------
REM Location of old path and new path
REM ------------------------------------------------------------------
SET old_path=W:\Documents\Projects\TapsRegressionChecker_3.0\Resource\Resource\TAPS_3.8_SNAPSHOT_1
SET new_path=W:\Documents\Projects\TapsRegressionChecker_3.0\Resource\Resource\TAPS_3.9_SNAPSHOT_1

REM ------------------------------------------------------------------
REM Location of jar file, for official use, use jar_releases.
REM ------------------------------------------------------------------
SET jar_debug=W:\Documents\Projects\TapsRegressionChecker_3.0\Code\build\libs\tapschecker.jar
SET jar_release=W:\Documents\Projects\TapsRegressionChecker_3.0\Release\version_3.0\tapschecker.jar

REM ------------------------------------------------------------------
REM Name of report file
REM ------------------------------------------------------------------
SET report_file=report_file.txt

REM ------------------------------------------------------------------
REM show = to show pass and error the checking
REM hide = to hide only error checking
REM ------------------------------------------------------------------
SET switch=show

REM ------------------------------------------------------------------
REM Taps checker execution
REM ------------------------------------------------------------------
java -jar %jar_release% %switch% %old_path% %new_path% > %report_file% 