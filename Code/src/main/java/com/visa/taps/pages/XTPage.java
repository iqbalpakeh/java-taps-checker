/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps.pages;

import java.io.File;

import com.visa.taps.Logger;
import com.visa.taps.data.XTData;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class XTPage extends HtmlPage {

    public final static String FILE_FILTER = "Interoperability - Results.html";

    public final static String NUMBER_OF_COLUMNS = "NUMBER_OF_COLUMNS";

    private static final int ORDER_VTF = 1;

    private static final int ORDER_VERDICT = 3;

    private static final int ORDER_LAB = 4; // need to get children

    private static final int ORDER_RESULT = 6; // need to get children

    private static final int ORDER_CORRECT = 8; // need to get children

    public XTPage(String rootFolder, String relativePath) {

        mRootFolder = rootFolder;
        mFileName = relativePath;
        
        Logger.debug("xtpage", rootFolder + relativePath);
        buildMap(rootFolder + relativePath);
    }

    private void buildMap(String absPath) {

        try {

            File input = new File(absPath);
            Document doc = Jsoup.parse(input, "UTF-8", "");
            Elements tbodys = doc.getElementsByTag("tbody");

            for (Element tbody : tbodys) {

                for (Element tr : tbody.children()) {

                    int order = 0;
                    XTData data = new XTData();

                    for (Element td : tr.children()) {

                        switch(order) {
                            
                            case ORDER_VTF: 
                                data.setVTF(td.html());
                                break;
                            
                            case ORDER_VERDICT: 
                                data.setVerdict(td.html());
                                break;
                            
                            case ORDER_LAB:
                                data.setLab(extractChildrenValue(td));
                                break;

                            case ORDER_RESULT: 
                                data.setResult(extractChildrenValue(td));
                                break;
                            
                            case ORDER_CORRECT: 
                                data.setCorrect(extractChildrenValue(td));
                                break;
                            
                            default: break;
                        }

                        order++;
                    }

                    Logger.debug("xtpage", "data.vtf = " + data.getVTF());
                    Logger.debug("xtpage", "data.verdict = " + data.getVerdict());
                    Logger.debug("xtpage", "data.lab = " + data.getLab());
                    Logger.debug("xtpage", "data.result = " + data.getResult());
                    Logger.debug("xtpage", "data.correct = " + data.getCorrect());
                    Logger.debug("xtpage", "NUMBER OF COLUMNS = " + order);
                    Logger.debug("xtpage", "\n");

                    mXTMap.put(data.getVTF(), data);
                    mSTMap.put(NUMBER_OF_COLUMNS, Integer.toString(order));
                }

                Logger.debug("xtpage", "\n");                

                Elements elements = doc.getAllElements();
                for (Element element : elements) {
                    if (element.id().equals("") == false) {
                        Logger.debug("element", "element_id = " + element.id());
                        mSTMap.put(element.id(), element.id());
                    }
                }
                
            }

        } catch(Exception exception) {
            Logger.warning("!!!!Exception : " + exception.getMessage());
        }

    }

    private String extractChildrenValue(Element element) {
        String value = "";
        for (Element p : element.getElementsByTag("p")) {
            value += p.html();
        }
        return value;
    }

}