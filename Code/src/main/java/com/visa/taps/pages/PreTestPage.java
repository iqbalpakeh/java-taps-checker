/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps.pages;

import java.io.File;

import com.visa.taps.Logger;
import com.visa.taps.data.Data;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PreTestPage extends HtmlPage {

    public final static String FILE_FILTER = "Interoperability - Pre Test.html";

    public final static String TITLE_SAMPLES_RECEIVED = "Sample Received";

    public final static String TITLE_TESTED_BY = "Tested By";

    public final static String TITLE_PRE_TEST_START = "Pre Test Start";

    public final static String TITLE_PRE_TEST_END = "Pre Test End";

    public final static String TITLE_PRE_TEST_RESULT = "Pre Test Result";

    public final static String TITLE_FILE_SHARE = "File Share";

    public final static String TITLE_CROSS_TESTING_HISTORY = "Cross Testing History";

    public final static String TITLE_PERFORMANCE_COMMENTS = "Performance Comments";

    public final static String TITLE_AVG_TIME_LAB = "Average Time Lab";
    
    public final static String TITLE_AVG_TIME_VISA = "Average Time Visa";
    
    public final static String TITLE_SELECT_PPSE_LAB_1 = "Select PPSE Lab 1";

    public final static String TITLE_SELECT_PPSE_LAB_2 = "Select PPSE Lab 2";
    
    public final static String TITLE_SELECT_PPSE_LAB_3 = "Select PPSE Lab 3";
    
    public final static String TITLE_SELECT_PPSE_LAB_4 = "Select PPSE Lab 4";

    public final static String TITLE_SELECT_ADF_LAB_1 = "Select ADF Lab 1";

    public final static String TITLE_SELECT_ADF_LAB_2 = "Select ADF Lab 2";

    public final static String TITLE_SELECT_ADF_LAB_3 = "Select ADF Lab 3";

    public final static String TITLE_SELECT_ADF_LAB_4 = "Select ADF Lab 4";

    public final static String TITLE_GPO_LAB_1 = "GPO LAB 1";

    public final static String TITLE_GPO_LAB_2 = "GPO LAB 2";

    public final static String TITLE_GPO_LAB_3 = "GPO LAB 3";

    public final static String TITLE_GPO_LAB_4 = "GPO LAB 4";

    public final static String TITLE_READ_RECORD_1_LAB_1 = "Read Record 1 Lab 1";

    public final static String TITLE_READ_RECORD_1_LAB_2 = "Read Record 1 Lab 2";

    public final static String TITLE_READ_RECORD_1_LAB_3 = "Read Record 1 Lab 3";

    public final static String TITLE_READ_RECORD_1_LAB_4 = "Read Record 1 Lab 4";

    public final static String TITLE_READ_RECORD_2_LAB_1 = "Read Record 2 Lab 1";

    public final static String TITLE_READ_RECORD_2_LAB_2 = "Read Record 2 Lab 2";

    public final static String TITLE_READ_RECORD_2_LAB_3 = "Read Record 2 Lab 3";

    public final static String TITLE_READ_RECORD_2_LAB_4 = "Read Record 2 Lab 4";

    public final static String TITLE_READ_RECORD_3_LAB_1 = "Read Record 3 Lab 1";

    public final static String TITLE_READ_RECORD_3_LAB_2 = "Read Record 3 Lab 2";

    public final static String TITLE_READ_RECORD_3_LAB_3 = "Read Record 3 Lab 3";

    public final static String TITLE_READ_RECORD_3_LAB_4 = "Read Record 3 Lab 4";

    public final static String TITLE_READ_RECORD_4_LAB_1 = "Read Record 4 Lab 1";

    public final static String TITLE_READ_RECORD_4_LAB_2 = "Read Record 4 Lab 2";

    public final static String TITLE_READ_RECORD_4_LAB_3 = "Read Record 4 Lab 3";

    public final static String TITLE_READ_RECORD_4_LAB_4 = "Read Record 4 Lab 4";

    public final static String TITLE_READ_RECORD_5_LAB_1 = "Read Record 5 Lab 1";

    public final static String TITLE_READ_RECORD_5_LAB_2 = "Read Record 5 Lab 2";

    public final static String TITLE_READ_RECORD_5_LAB_3 = "Read Record 5 Lab 3";

    public final static String TITLE_READ_RECORD_5_LAB_4 = "Read Record 5 Lab 4";
    
    public final static String TITLE_SELECT_PPSE_VISA_1 = "Select PPSE Visa 1";

    public final static String TITLE_SELECT_PPSE_VISA_2 = "Select PPSE Visa 2";
    
    public final static String TITLE_SELECT_ADF_VISA_1 = "Select ADF Visa 1";

    public final static String TITLE_SELECT_ADF_VISA_2 = "Select ADF Visa 2";

    public final static String TITLE_GPO_VISA_1 = "GPO Visa 1";

    public final static String TITLE_GPO_VISA_2 = "GPO Visa 2";
    
    public final static String TITLE_READ_RECORD_1_VISA_1 = "Read Record 1 Visa 1";

    public final static String TITLE_READ_RECORD_1_VISA_2 = "Read Record 1 Visa 2";

    public final static String TITLE_READ_RECORD_2_VISA_1 = "Read Record 2 Visa 1";

    public final static String TITLE_READ_RECORD_2_VISA_2 = "Read Record 2 Visa 2";

    public final static String TITLE_READ_RECORD_3_VISA_1 = "Read Record 3 Visa 1";

    public final static String TITLE_READ_RECORD_3_VISA_2 = "Read Record 3 Visa 2";

    public final static String TITLE_READ_RECORD_4_VISA_1 = "Read Record 4 Visa 1";

    public final static String TITLE_READ_RECORD_4_VISA_2 = "Read Record 4 Visa 2";

    public final static String TITLE_READ_RECORD_5_VISA_1 = "Read Record 5 Visa 1";

    public final static String TITLE_READ_RECORD_5_VISA_2 = "Read Record 5 Visa 2";        

    public final static String ID_SAMPLES_RECEIVED = "PreTest_SamplesReceived";

    public final static String ID_TESTED_BY = "PreTest_TestedBy";

    public final static String ID_PRE_TEST_START = "PreTest_PreTestStart";

    public final static String ID_PRE_TEST_END = "PreTest_PreTestEnd";

    public final static String ID_PRE_TEST_RESULT = "PreTest_PreTestResult";

    public final static String ID_FILE_SHARE = "FileShare";

    public final static String ID_CROSS_TESTING_HISTORY = "CrossTestingHistory";

    public final static String ID_PERFORMANCE_COMMENTS = "PreTest_PerformanceComment";

    public final static String ID_AVG_TIME_LAB = "PreTest_AverageLabTiming";
    
    public final static String ID_AVG_TIME_VISA = "PreTest_AverageVisaTimingDevice";
    
    public final static String ID_SELECT_PPSE_LAB_1 = "PerformanceDevice_0__LabTiming1";

    public final static String ID_SELECT_PPSE_LAB_2 = "PerformanceDevice_0__LabTiming2";
    
    public final static String ID_SELECT_PPSE_LAB_3 = "PerformanceDevice_0__LabTiming3";
    
    public final static String ID_SELECT_PPSE_LAB_4 = "PerformanceDevice_0__LabTiming4";

    public final static String ID_SELECT_ADF_LAB_1 = "PerformanceDevice_1__LabTiming1";

    public final static String ID_SELECT_ADF_LAB_2 = "PerformanceDevice_1__LabTiming2";

    public final static String ID_SELECT_ADF_LAB_3 = "PerformanceDevice_1__LabTiming3";

    public final static String ID_SELECT_ADF_LAB_4 = "PerformanceDevice_1__LabTiming4";

    public final static String ID_GPO_LAB_1 = "PerformanceDevice_2__LabTiming1";

    public final static String ID_GPO_LAB_2 = "PerformanceDevice_2__LabTiming2";

    public final static String ID_GPO_LAB_3 = "PerformanceDevice_2__LabTiming3";

    public final static String ID_GPO_LAB_4 = "PerformanceDevice_2__LabTiming4";

    public final static String ID_READ_RECORD_1_LAB_1 = "PerformanceDevice_3__LabTiming1";

    public final static String ID_READ_RECORD_1_LAB_2 = "PerformanceDevice_3__LabTiming2";

    public final static String ID_READ_RECORD_1_LAB_3 = "PerformanceDevice_3__LabTiming3";

    public final static String ID_READ_RECORD_1_LAB_4 = "PerformanceDevice_3__LabTiming4";

    public final static String ID_READ_RECORD_2_LAB_1 = "PerformanceDevice_4__LabTiming1";

    public final static String ID_READ_RECORD_2_LAB_2 = "PerformanceDevice_4__LabTiming2";

    public final static String ID_READ_RECORD_2_LAB_3 = "PerformanceDevice_4__LabTiming3";

    public final static String ID_READ_RECORD_2_LAB_4 = "PerformanceDevice_4__LabTiming4";

    public final static String ID_READ_RECORD_3_LAB_1 = "PerformanceDevice_5__LabTiming1";

    public final static String ID_READ_RECORD_3_LAB_2 = "PerformanceDevice_5__LabTiming2";

    public final static String ID_READ_RECORD_3_LAB_3 = "PerformanceDevice_5__LabTiming3";

    public final static String ID_READ_RECORD_3_LAB_4 = "PerformanceDevice_5__LabTiming4";

    public final static String ID_READ_RECORD_4_LAB_1 = "PerformanceDevice_6__LabTiming1";

    public final static String ID_READ_RECORD_4_LAB_2 = "PerformanceDevice_6__LabTiming2";

    public final static String ID_READ_RECORD_4_LAB_3 = "PerformanceDevice_6__LabTiming3";

    public final static String ID_READ_RECORD_4_LAB_4 = "PerformanceDevice_6__LabTiming4";

    public final static String ID_READ_RECORD_5_LAB_1 = "PerformanceDevice_7__LabTiming1";

    public final static String ID_READ_RECORD_5_LAB_2 = "PerformanceDevice_7__LabTiming2";

    public final static String ID_READ_RECORD_5_LAB_3 = "PerformanceDevice_7__LabTiming3";

    public final static String ID_READ_RECORD_5_LAB_4 = "PerformanceDevice_7__LabTiming4";  

    public final static String ID_SELECT_PPSE_VISA_1 = "PerformanceDevice_0__VisaTiming1";

    public final static String ID_SELECT_PPSE_VISA_2 = "PerformanceDevice_0__VisaTiming2";
    
    public final static String ID_SELECT_ADF_VISA_1 = "PerformanceDevice_1__VisaTiming1";

    public final static String ID_SELECT_ADF_VISA_2 = "PerformanceDevice_1__VisaTiming2";

    public final static String ID_GPO_VISA_1 = "PerformanceDevice_2__VisaTiming1";

    public final static String ID_GPO_VISA_2 = "PerformanceDevice_2__VisaTiming2";
    
    public final static String ID_READ_RECORD_1_VISA_1 = "PerformanceDevice_3__VisaTiming1";

    public final static String ID_READ_RECORD_1_VISA_2 = "PerformanceDevice_3__VisaTiming2";

    public final static String ID_READ_RECORD_2_VISA_1 = "PerformanceDevice_4__VisaTiming1";

    public final static String ID_READ_RECORD_2_VISA_2 = "PerformanceDevice_4__VisaTiming2";

    public final static String ID_READ_RECORD_3_VISA_1 = "PerformanceDevice_5__VisaTiming1";

    public final static String ID_READ_RECORD_3_VISA_2 = "PerformanceDevice_5__VisaTiming2";

    public final static String ID_READ_RECORD_4_VISA_1 = "PerformanceDevice_6__VisaTiming1";

    public final static String ID_READ_RECORD_4_VISA_2 = "PerformanceDevice_6__VisaTiming2";

    public final static String ID_READ_RECORD_5_VISA_1 = "PerformanceDevice_7__VisaTiming1";

    public final static String ID_READ_RECORD_5_VISA_2 = "PerformanceDevice_7__VisaTiming2";    

    public PreTestPage(String rootFolder, String relativePath) {

        mRootFolder = rootFolder;
        mFileName = relativePath;

        buildMap(rootFolder + relativePath);
    }

    private Data extractPage(Document doc, String id, String title) {        
        Data data;
        switch(id) {
            case ID_CROSS_TESTING_HISTORY:
                data = new Data(id, title, extractTextArea(doc, id));
                break;
            default:
                data = new Data(id, title, extractValue(doc, id));
                break;
        }
        return data;
    }

    private void buildMap(String absPath) {

        try {

            File input = new File(absPath);
            Document doc = Jsoup.parse(input, "UTF-8", "");

	        mMap.put(ID_SAMPLES_RECEIVED, extractPage(doc, ID_SAMPLES_RECEIVED, TITLE_SAMPLES_RECEIVED));

            mMap.put(ID_TESTED_BY, extractPage(doc, ID_TESTED_BY, TITLE_TESTED_BY));

	        mMap.put(ID_PRE_TEST_START, extractPage(doc, ID_PRE_TEST_START, TITLE_PRE_TEST_START));

            mMap.put(ID_PRE_TEST_END, extractPage(doc, ID_PRE_TEST_END, TITLE_PRE_TEST_END));
            
            mMap.put(ID_PRE_TEST_RESULT, extractPage(doc, ID_PRE_TEST_RESULT, TITLE_PRE_TEST_RESULT));

            mMap.put(ID_FILE_SHARE, extractPage(doc, ID_FILE_SHARE, TITLE_FILE_SHARE));

            mMap.put(ID_CROSS_TESTING_HISTORY, extractPage(doc, ID_CROSS_TESTING_HISTORY, TITLE_CROSS_TESTING_HISTORY));

            mMap.put(ID_PERFORMANCE_COMMENTS, extractPage(doc, ID_PERFORMANCE_COMMENTS, TITLE_PERFORMANCE_COMMENTS));

            mMap.put(ID_AVG_TIME_LAB, extractPage(doc, ID_AVG_TIME_LAB, TITLE_AVG_TIME_LAB));
        
            mMap.put(ID_AVG_TIME_VISA, extractPage(doc, ID_AVG_TIME_VISA, TITLE_AVG_TIME_VISA));
        
            mMap.put(ID_SELECT_PPSE_LAB_1, extractPage(doc, ID_SELECT_PPSE_LAB_1, TITLE_SELECT_PPSE_LAB_1));

            mMap.put(ID_SELECT_PPSE_LAB_2, extractPage(doc, ID_SELECT_PPSE_LAB_2, TITLE_SELECT_PPSE_LAB_2));
        
            mMap.put(ID_SELECT_PPSE_LAB_3, extractPage(doc, ID_SELECT_PPSE_LAB_3, TITLE_SELECT_PPSE_LAB_3));
        
            mMap.put(ID_SELECT_PPSE_LAB_4, extractPage(doc, ID_SELECT_PPSE_LAB_4, TITLE_SELECT_PPSE_LAB_4));

            mMap.put(ID_SELECT_ADF_LAB_1, extractPage(doc, ID_SELECT_ADF_LAB_1, TITLE_SELECT_ADF_LAB_1));

            mMap.put(ID_SELECT_ADF_LAB_2, extractPage(doc, ID_SELECT_ADF_LAB_2, TITLE_SELECT_ADF_LAB_2));

            mMap.put(ID_SELECT_ADF_LAB_3, extractPage(doc, ID_SELECT_ADF_LAB_3, TITLE_SELECT_ADF_LAB_3));

            mMap.put(ID_SELECT_ADF_LAB_4, extractPage(doc, ID_SELECT_ADF_LAB_4, TITLE_SELECT_ADF_LAB_4));

            mMap.put(ID_GPO_LAB_1, extractPage(doc, ID_GPO_LAB_1, TITLE_GPO_LAB_1));

            mMap.put(ID_GPO_LAB_2, extractPage(doc, ID_GPO_LAB_2, TITLE_GPO_LAB_2));

            mMap.put(ID_GPO_LAB_3, extractPage(doc, ID_GPO_LAB_3, TITLE_GPO_LAB_3));

            mMap.put(ID_GPO_LAB_4, extractPage(doc, ID_GPO_LAB_4, TITLE_GPO_LAB_4));

            mMap.put(ID_READ_RECORD_1_LAB_1, extractPage(doc, ID_READ_RECORD_1_LAB_1, TITLE_READ_RECORD_1_LAB_1));

            mMap.put(ID_READ_RECORD_1_LAB_2, extractPage(doc, ID_READ_RECORD_1_LAB_2, TITLE_READ_RECORD_1_LAB_2));

            mMap.put(ID_READ_RECORD_1_LAB_3, extractPage(doc, ID_READ_RECORD_1_LAB_3, TITLE_READ_RECORD_1_LAB_3));

            mMap.put(ID_READ_RECORD_1_LAB_4, extractPage(doc, ID_READ_RECORD_1_LAB_4, TITLE_READ_RECORD_1_LAB_4));

            mMap.put(ID_READ_RECORD_2_LAB_1, extractPage(doc, ID_READ_RECORD_2_LAB_1, TITLE_READ_RECORD_2_LAB_1));

            mMap.put(ID_READ_RECORD_2_LAB_2, extractPage(doc, ID_READ_RECORD_2_LAB_2, TITLE_READ_RECORD_2_LAB_2));            

            mMap.put(ID_READ_RECORD_2_LAB_3, extractPage(doc, ID_READ_RECORD_2_LAB_3, TITLE_READ_RECORD_2_LAB_3));            

            mMap.put(ID_READ_RECORD_2_LAB_4, extractPage(doc, ID_READ_RECORD_2_LAB_4, TITLE_READ_RECORD_2_LAB_4));                        

            mMap.put(ID_READ_RECORD_3_LAB_1, extractPage(doc, ID_READ_RECORD_3_LAB_1, TITLE_READ_RECORD_3_LAB_1));                        

            mMap.put(ID_READ_RECORD_3_LAB_2, extractPage(doc, ID_READ_RECORD_3_LAB_2, TITLE_READ_RECORD_3_LAB_2));                                    

            mMap.put(ID_READ_RECORD_3_LAB_3, extractPage(doc, ID_READ_RECORD_3_LAB_3, TITLE_READ_RECORD_3_LAB_3));                                    

            mMap.put(ID_READ_RECORD_3_LAB_4, extractPage(doc, ID_READ_RECORD_3_LAB_4, TITLE_READ_RECORD_3_LAB_4));             

            mMap.put(ID_READ_RECORD_4_LAB_1, extractPage(doc, ID_READ_RECORD_4_LAB_1, TITLE_READ_RECORD_4_LAB_1));                         

            mMap.put(ID_READ_RECORD_4_LAB_2, extractPage(doc, ID_READ_RECORD_4_LAB_2, TITLE_READ_RECORD_4_LAB_2));

            mMap.put(ID_READ_RECORD_4_LAB_3, extractPage(doc, ID_READ_RECORD_4_LAB_3, TITLE_READ_RECORD_4_LAB_3));

            mMap.put(ID_READ_RECORD_4_LAB_4, extractPage(doc, ID_READ_RECORD_4_LAB_4, TITLE_READ_RECORD_4_LAB_4));

            mMap.put(ID_READ_RECORD_5_LAB_1, extractPage(doc, ID_READ_RECORD_5_LAB_1, TITLE_READ_RECORD_5_LAB_1));

            mMap.put(ID_READ_RECORD_5_LAB_2, extractPage(doc, ID_READ_RECORD_5_LAB_2, TITLE_READ_RECORD_5_LAB_2));

            mMap.put(ID_READ_RECORD_5_LAB_3, extractPage(doc, ID_READ_RECORD_5_LAB_3, TITLE_READ_RECORD_5_LAB_3));

            mMap.put(ID_READ_RECORD_5_LAB_4, extractPage(doc, ID_READ_RECORD_5_LAB_4, TITLE_READ_RECORD_5_LAB_4));
        
            mMap.put(ID_SELECT_PPSE_VISA_1, extractPage(doc, ID_SELECT_PPSE_VISA_1, TITLE_SELECT_PPSE_VISA_1));

            mMap.put(ID_SELECT_PPSE_VISA_2, extractPage(doc, ID_SELECT_PPSE_VISA_2, TITLE_SELECT_PPSE_VISA_2));
            
            mMap.put(ID_SELECT_ADF_VISA_1, extractPage(doc, ID_SELECT_ADF_VISA_1, TITLE_SELECT_ADF_VISA_1));
        
            mMap.put(ID_SELECT_ADF_VISA_2, extractPage(doc, ID_SELECT_ADF_VISA_2, TITLE_SELECT_ADF_VISA_2));
        
            mMap.put(ID_GPO_VISA_1, extractPage(doc, ID_GPO_VISA_1, TITLE_GPO_VISA_1));
        
            mMap.put(ID_GPO_VISA_2, extractPage(doc, ID_GPO_VISA_2, TITLE_GPO_VISA_2));
            
            mMap.put(ID_READ_RECORD_1_VISA_1, extractPage(doc, ID_READ_RECORD_1_VISA_1, TITLE_READ_RECORD_1_VISA_1));

            mMap.put(ID_READ_RECORD_1_VISA_2, extractPage(doc, ID_READ_RECORD_1_VISA_2, TITLE_READ_RECORD_1_VISA_2));

            mMap.put(ID_READ_RECORD_2_VISA_1, extractPage(doc, ID_READ_RECORD_2_VISA_1, TITLE_READ_RECORD_2_VISA_1));

            mMap.put(ID_READ_RECORD_2_VISA_2, extractPage(doc, ID_READ_RECORD_2_VISA_2, TITLE_READ_RECORD_2_VISA_2));

            mMap.put(ID_READ_RECORD_3_VISA_1, extractPage(doc, ID_READ_RECORD_3_VISA_1, TITLE_READ_RECORD_3_VISA_1));

            mMap.put(ID_READ_RECORD_3_VISA_2, extractPage(doc, ID_READ_RECORD_3_VISA_2, TITLE_READ_RECORD_3_VISA_2));

            mMap.put(ID_READ_RECORD_4_VISA_1, extractPage(doc, ID_READ_RECORD_4_VISA_1, TITLE_READ_RECORD_4_VISA_1));

            mMap.put(ID_READ_RECORD_4_VISA_2, extractPage(doc, ID_READ_RECORD_4_VISA_2, TITLE_READ_RECORD_4_VISA_2));

            mMap.put(ID_READ_RECORD_5_VISA_1, extractPage(doc, ID_READ_RECORD_5_VISA_1, TITLE_READ_RECORD_5_VISA_1));

            mMap.put(ID_READ_RECORD_5_VISA_2, extractPage(doc, ID_READ_RECORD_5_VISA_2, TITLE_READ_RECORD_5_VISA_2));

            Elements elements = doc.getAllElements();
            for (Element element : elements) {
                if (element.id().equals("") == false) {
                    Logger.debug("element", "element_id = " + element.id());
                    mSTMap.put(element.id(), element.id());
                }
            }

        } catch(Exception exception) {
            Logger.warning("!!!!Exception : " + exception.getMessage());
        }
    }
}