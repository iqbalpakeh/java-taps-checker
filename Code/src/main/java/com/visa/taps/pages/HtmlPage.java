/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps.pages;

import com.visa.taps.data.Data;
import com.visa.taps.data.XTData;

import java.util.TreeMap;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HtmlPage {

    protected String mRootFolder;

    protected String mFileName;

    protected TreeMap<String, Data> mMap;

    protected TreeMap<String, XTData> mXTMap;

    protected TreeMap<String, String> mSTMap;

    public HtmlPage() {
        mMap = new TreeMap<>();
        mXTMap = new TreeMap<>();
        mSTMap = new TreeMap<>();
    }

    protected String extractValue(Document doc, String id) {
        return doc.getElementById(id).val();
    }

    protected String extractTextArea(Document doc, String id) {
        String result = "";
        Elements elements = doc.select("textarea[id^=" + id + "]");
        for (Element element : elements) {
            // This operation is required to remove formating different on the extracted String 
            // to avoid incorrect comparison!!!
            result += element.html().replace("\n", "").replace(" ", "").trim();
        }
        return result;
    }

    public String getFullPath() {
        return mRootFolder + mFileName;
    }

    public String getFileName() {
        return mFileName;
    }

    public Data getDataByKey(String key) {
        return mMap.get(key);
    }

    public TreeMap<String, Data> getMap() {
        return mMap;
    }

    public XTData getXTDataByKey(String key) {
        return mXTMap.get(key);
    }

    public TreeMap<String, XTData> getXTMap() {
        return mXTMap;
    }

    public String getSTDataByKey(String key) {
        return mSTMap.get(key);
    }

    public TreeMap<String, String> getSTMap() {
        return mSTMap;
    }

}