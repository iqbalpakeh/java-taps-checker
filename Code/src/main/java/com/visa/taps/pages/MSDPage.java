/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps.pages;

import java.io.File;

import com.visa.taps.Logger;
import com.visa.taps.data.Data;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MSDPage extends HtmlPage {

    public final static String FILE_FILTER = "MSD.html";

    public final static String TITLE_TESTING_SCOPE = "Testing Scope";

    public final static String TITLE_LAB = "Lab Name";

    public final static String TITLE_TESTING_REQUIREMENT_COMMENTS = "Testing Requirement Comments";

    public final static String TITLE_REPORT_NUMBER = "Report Number";

    public final static String TITLE_TEST_PLAN_VERSION = "Test Plan Version";

    public final static String TITLE_LAB_TESTING_STARTED = "Lab Testing Started";

    public final static String TITLE_LAB_TESTING_FINISHED = "Lab Testing Finished";

    public final static String TITLE_REPORT_REVIEW_HISTORY = "Report Review History";

    public final static String TITLE_REPORT_REVIEWER = "Report Reviewer";

    public final static String TITLE_REPORT_REVIEWER_ASSIGNED_DATE = "Report Reviewer Assigned Date";

    public final static String TITLE_TEST_REPORT_RECEIVED_DATE = "Test Report Received Date";

    public final static String TITLE_REVIEW_STARTED_DATE = "Review Started Date";

    public final static String TITLE_ISSUES_COUNT = "Issues Count";

    public final static String TITLE_ISSUES_DESCRIPTION = "Issues Description";

    public final static String TITLE_REPORT_REVIEW_COMMENTS = "Report Review Comments";

    public final static String TITLE_WHO_TO_NOTIFY = "Who To Notify";

    public final static String TITLE_REVIEW_COMPLETED_DATE = "Review Completed Date";

    public final static String TITLE_OVERALL_RESULT = "Overall Result";

    public final static String TITLE_COMPLETED_DATE = "Completed Date";

    public final static String ID_TESTING_SCOPE = "TestingRequirement_CategoryScopeId";

    public final static String ID_LAB = "TestingRequirement_LabId";

    public final static String ID_TESTING_REQUIREMENT_COMMENTS = "TestingRequirement_Comments";

    public final static String ID_REPORT_NUMBER = "LabReport_ReportNumber";

    public final static String ID_TEST_PLAN_VERSION = "LabReport_TestPlanVersion";

    public final static String ID_LAB_TESTING_STARTED = "LabReport_LabStartDate";

    public final static String ID_LAB_TESTING_FINISHED = "LabReport_LabEndDate";

    public final static String ID_REPORT_REVIEW_HISTORY = "ReportReview_ReportReviewHistory";

    public final static String ID_REPORT_REVIEWER = "ReportReview_ReportReviewer";

    public final static String ID_REPORT_REVIEWER_ASSIGNED_DATE = "ReportReview_ReviewerAssignedDate";

    public final static String ID_TEST_REPORT_RECEIVED_DATE = "LabReport_ReportReceivedDate";

    public final static String ID_REVIEW_STARTED_DATE = "ReportReview_ReportReviewStartDate";

    public final static String ID_ISSUES_COUNT = "ReportReview_IssuesCount";

    public final static String ID_ISSUES_DESCRIPTION = "ReportReview_IssuesDescription";

    public final static String ID_REPORT_REVIEW_COMMENTS = "ReportReview_ReviewerComment";

    public final static String ID_WHO_TO_NOTIFY = "ReportReview_ReviewerManager";

    public final static String ID_REVIEW_COMPLETED_DATE = "ReportReview_ReportReviewEndDate";

    public final static String ID_OVERALL_RESULT = "ReportReview_OverallResult";

    public final static String ID_COMPLETED_DATE = "ReportReview_ReviewCompletedDate";

    public MSDPage(String rootFolder, String relativePath) {
        mRootFolder = rootFolder;
        mFileName = relativePath;
        buildMap(rootFolder + relativePath);
    }

    private Data extractPage(Document doc, String id, String title) {        
        Data data;
        switch(id) {
            case ID_REPORT_REVIEW_HISTORY:
            case ID_ISSUES_DESCRIPTION:
            case ID_REPORT_REVIEW_COMMENTS:
                data = new Data(id, title, extractTextArea(doc, id));
                break;
            default:
                data = new Data(id, title, extractValue(doc, id));
                break;
        }
        return data;
    }

    private void buildMap(String absPath) {

        try {

            File input = new File(absPath);
            Document doc = Jsoup.parse(input, "UTF-8", "");

            mMap.put(ID_TESTING_SCOPE, extractPage(doc, ID_TESTING_SCOPE, TITLE_TESTING_SCOPE));

            mMap.put(ID_LAB, extractPage(doc, ID_LAB, TITLE_LAB));
                
            mMap.put(ID_TESTING_REQUIREMENT_COMMENTS, extractPage(doc, ID_TESTING_REQUIREMENT_COMMENTS, TITLE_TESTING_REQUIREMENT_COMMENTS));
                
            mMap.put(ID_REPORT_NUMBER, extractPage(doc, ID_REPORT_NUMBER, TITLE_REPORT_NUMBER));
            
            mMap.put(ID_TEST_PLAN_VERSION, extractPage(doc, ID_TEST_PLAN_VERSION, TITLE_TEST_PLAN_VERSION));
            
            mMap.put(ID_LAB_TESTING_STARTED, extractPage(doc, ID_LAB_TESTING_STARTED, TITLE_LAB_TESTING_STARTED));
            
            mMap.put(ID_LAB_TESTING_FINISHED, extractPage(doc, ID_LAB_TESTING_FINISHED, TITLE_LAB_TESTING_FINISHED));
            
            mMap.put(ID_REPORT_REVIEW_HISTORY, extractPage(doc, ID_REPORT_REVIEW_HISTORY, TITLE_REPORT_REVIEW_HISTORY));

            mMap.put(ID_REPORT_REVIEWER, extractPage(doc, ID_REPORT_REVIEWER, TITLE_REPORT_REVIEWER));
            
            mMap.put(ID_REPORT_REVIEWER_ASSIGNED_DATE, extractPage(doc, ID_REPORT_REVIEWER_ASSIGNED_DATE, TITLE_REPORT_REVIEWER_ASSIGNED_DATE));
            
            mMap.put(ID_TEST_REPORT_RECEIVED_DATE, extractPage(doc, ID_TEST_REPORT_RECEIVED_DATE, TITLE_TEST_REPORT_RECEIVED_DATE));
            
            mMap.put(ID_REVIEW_STARTED_DATE, extractPage(doc, ID_REVIEW_STARTED_DATE, TITLE_REVIEW_STARTED_DATE));
            
            mMap.put(ID_ISSUES_COUNT, extractPage(doc, ID_ISSUES_COUNT, TITLE_ISSUES_COUNT));
            
            mMap.put(ID_ISSUES_DESCRIPTION, extractPage(doc, ID_ISSUES_DESCRIPTION, TITLE_ISSUES_DESCRIPTION));
            
            mMap.put(ID_REPORT_REVIEW_COMMENTS, extractPage(doc, ID_REPORT_REVIEW_COMMENTS, TITLE_REPORT_REVIEW_COMMENTS));
            
            mMap.put(ID_WHO_TO_NOTIFY, extractPage(doc, ID_WHO_TO_NOTIFY, TITLE_WHO_TO_NOTIFY));
            
            mMap.put(ID_REVIEW_COMPLETED_DATE, extractPage(doc, ID_REVIEW_COMPLETED_DATE, TITLE_REVIEW_COMPLETED_DATE));
            
            mMap.put(ID_OVERALL_RESULT, extractPage(doc, ID_OVERALL_RESULT, TITLE_OVERALL_RESULT));
            
            mMap.put(ID_COMPLETED_DATE, extractPage(doc, ID_COMPLETED_DATE, TITLE_COMPLETED_DATE));

            Elements elements = doc.getAllElements();
            for (Element element : elements) {
                if (element.id().equals("") == false) {
                    Logger.debug("element", "element_id = " + element.id());
                    mSTMap.put(element.id(), element.id());
                }
            }

        } catch(Exception exception) {
            Logger.warning("!!!!Exception : " + exception.getMessage());
        }
    }

 }