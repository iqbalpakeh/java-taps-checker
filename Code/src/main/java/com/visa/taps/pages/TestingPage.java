/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps.pages;

import java.io.File;

import com.visa.taps.Logger;
import com.visa.taps.data.Data;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TestingPage extends HtmlPage {

    public final static String FILE_FILTER = "Interoperability - Testing.html";

    public static final String TITLE_TESTING_SCOPE = "Testing Scope";

    public static final String TITLE_LAB_NAME = "Lab";

    public static final String TITLE_TESTING_REQUIREMENT_COMMENTS = "Testing Requirement Comments";

    public static final String TITLE_REPORT_NUMBER = "Report Number";

    public static final String TITLE_TEST_REPORT_RECEIVED = "Test Report Received";

    public static final String TITLE_LAB_TESTING_STARTED = "Lab Testing Started";

    public static final String TITLE_LAB_TESTING_FINISHED = "Lab Testing Finished";

    public static final String TITLE_TESTER = "Tester";

    public static final String TITLE_TEST_SHEET = "Test Sheet";

    public static final String TITLE_CROSS_TESTING_STARTED = "Testing Started";

    public static final String TITLE_CROSS_TESTING_FINISHED = "Testing Finished";

    public static final String TITLE_CROSS_TESTING_COMMENTS = "Comments";

    public static final String TITLE_SHEET_CREATED = "Sheet Created";

    public static final String TITLE_PERFORMANCE_AVG_LAB = "Avg Time (Lab)";

    public static final String TITLE_SELECT_PPSE_LAB_1 = "Select PPSE Lab 1";

    public static final String TITLE_SELECT_PPSE_LAB_2 = "Select PPSE Lab 2";

    public static final String TITLE_SELECT_PPSE_LAB_3 = "Select PPSE Lab 3";

    public static final String TITLE_SELECT_PPSE_LAB_4 = "Select PPSE Lab 4";

    public static final String TITLE_SELECT_ADF_LAB_1 = "Select ADF Lab 1";

    public static final String TITLE_SELECT_ADF_LAB_2 = "Select ADF Lab 2";

    public static final String TITLE_SELECT_ADF_LAB_3 = "Select ADF Lab 3";
    
    public static final String TITLE_SELECT_ADF_LAB_4 = "Select ADF Lab 4";

    public static final String TITLE_GPO_LAB_1 = "GPO Lab 1";

    public static final String TITLE_GPO_LAB_2 = "GPO Lab 2";

    public static final String TITLE_GPO_LAB_3 = "GPO Lab 3";

    public static final String TITLE_GPO_LAB_4 = "GPO Lab 4";

    public static final String TITLE_READ_RECORD_1_LAB_1 = "Read Record 1 Lab 1";

    public static final String TITLE_READ_RECORD_1_LAB_2 = "Read Record 1 Lab 2";

    public static final String TITLE_READ_RECORD_1_LAB_3 = "Read Record 1 Lab 3";

    public static final String TITLE_READ_RECORD_1_LAB_4 = "Read Record 1 Lab 4";

    public static final String TITLE_READ_RECORD_2_LAB_1 = "Read Record 2 Lab 1";

    public static final String TITLE_READ_RECORD_2_LAB_2 = "Read Record 2 Lab 2";

    public static final String TITLE_READ_RECORD_2_LAB_3 = "Read Record 2 Lab 3";

    public static final String TITLE_READ_RECORD_2_LAB_4 = "Read Record 2 Lab 4";

    public static final String TITLE_READ_RECORD_3_LAB_1 = "Read Record 3 Lab 1";

    public static final String TITLE_READ_RECORD_3_LAB_2 = "Read Record 3 Lab 2";

    public static final String TITLE_READ_RECORD_3_LAB_3 = "Read Record 3 Lab 3";

    public static final String TITLE_READ_RECORD_3_LAB_4 = "Read Record 3 Lab 4";    

    public static final String TITLE_READ_RECORD_4_LAB_1 = "Read Record 4 Lab 1";

    public static final String TITLE_READ_RECORD_4_LAB_2 = "Read Record 4 Lab 2";

    public static final String TITLE_READ_RECORD_4_LAB_3 = "Read Record 4 Lab 3";

    public static final String TITLE_READ_RECORD_4_LAB_4 = "Read Record 4 Lab 4";     

    public static final String TITLE_READ_RECORD_5_LAB_1 = "Read Record 5 Lab 1";

    public static final String TITLE_READ_RECORD_5_LAB_2 = "Read Record 5 Lab 2";

    public static final String TITLE_READ_RECORD_5_LAB_3 = "Read Record 5 Lab 3";

    public static final String TITLE_READ_RECORD_5_LAB_4 = "Read Record 5 Lab 4"; 

    public static final String ID_TESTING_SCOPE = "TestingRequirement_CategoryScopeId";

    public static final String ID_LAB_NAME = "TestingRequirement_LabId";

    public static final String ID_TESTING_REQUIREMENT_COMMENTS = "TestingRequirement_Comments";

    public static final String ID_REPORT_NUMBER = "LabReport_ReportNumber";

    public static final String ID_TEST_REPORT_RECEIVED = "LabReport_ReportReceivedDate";

    public static final String ID_LAB_TESTING_STARTED = "LabReport_LabStartDate";

    public static final String ID_LAB_TESTING_FINISHED = "LabReport_LabEndDate";

    public static final String ID_TESTER = "ReportReview_CrossTester";

    public static final String ID_TEST_SHEET = "ReportReview_PoolId";

    public static final String ID_CROSS_TESTING_STARTED = "ReportReview_CrossTestingStartDate";

    public static final String ID_CROSS_TESTING_FINISHED = "ReportReview_CrossTestingEndDate";

    public static final String ID_CROSS_TESTING_COMMENTS = "ReportReview_CrossTestingComments";

    public static final String ID_SHEET_CREATED = "ReportReview_SheetCreateDate";

    public static final String ID_PERFORMANCE_AVG_LAB = "PreTest_AverageLabTiming";

    public static final String ID_SELECT_PPSE_LAB_1 = "PerformanceDevice_0__LabTiming1";

    public static final String ID_SELECT_PPSE_LAB_2 = "PerformanceDevice_0__LabTiming2";

    public static final String ID_SELECT_PPSE_LAB_3 = "PerformanceDevice_0__LabTiming3";

    public static final String ID_SELECT_PPSE_LAB_4 = "PerformanceDevice_0__LabTiming4";

    public static final String ID_SELECT_ADF_LAB_1 = "PerformanceDevice_1__LabTiming1";

    public static final String ID_SELECT_ADF_LAB_2 = "PerformanceDevice_1__LabTiming2";

    public static final String ID_SELECT_ADF_LAB_3 = "PerformanceDevice_1__LabTiming3";
    
    public static final String ID_SELECT_ADF_LAB_4 = "PerformanceDevice_1__LabTiming4";

    public static final String ID_GPO_LAB_1 = "PerformanceDevice_2__LabTiming1";

    public static final String ID_GPO_LAB_2 = "PerformanceDevice_2__LabTiming2";

    public static final String ID_GPO_LAB_3 = "PerformanceDevice_2__LabTiming3";

    public static final String ID_GPO_LAB_4 = "PerformanceDevice_2__LabTiming4";

    public static final String ID_READ_RECORD_1_LAB_1 = "PerformanceDevice_3__LabTiming1";

    public static final String ID_READ_RECORD_1_LAB_2 = "PerformanceDevice_3__LabTiming2";

    public static final String ID_READ_RECORD_1_LAB_3 = "PerformanceDevice_3__LabTiming3";

    public static final String ID_READ_RECORD_1_LAB_4 = "PerformanceDevice_3__LabTiming4";

    public static final String ID_READ_RECORD_2_LAB_1 = "PerformanceDevice_4__LabTiming1";

    public static final String ID_READ_RECORD_2_LAB_2 = "PerformanceDevice_4__LabTiming2";

    public static final String ID_READ_RECORD_2_LAB_3 = "PerformanceDevice_4__LabTiming3";

    public static final String ID_READ_RECORD_2_LAB_4 = "PerformanceDevice_4__LabTiming4";

    public static final String ID_READ_RECORD_3_LAB_1 = "PerformanceDevice_5__LabTiming1";

    public static final String ID_READ_RECORD_3_LAB_2 = "PerformanceDevice_5__LabTiming2";

    public static final String ID_READ_RECORD_3_LAB_3 = "PerformanceDevice_5__LabTiming3";

    public static final String ID_READ_RECORD_3_LAB_4 = "PerformanceDevice_5__LabTiming4";    

    public static final String ID_READ_RECORD_4_LAB_1 = "PerformanceDevice_6__LabTiming1";

    public static final String ID_READ_RECORD_4_LAB_2 = "PerformanceDevice_6__LabTiming2";

    public static final String ID_READ_RECORD_4_LAB_3 = "PerformanceDevice_6__LabTiming3";

    public static final String ID_READ_RECORD_4_LAB_4 = "PerformanceDevice_6__LabTiming4";     

    public static final String ID_READ_RECORD_5_LAB_1 = "PerformanceDevice_7__LabTiming1";

    public static final String ID_READ_RECORD_5_LAB_2 = "PerformanceDevice_7__LabTiming2";

    public static final String ID_READ_RECORD_5_LAB_3 = "PerformanceDevice_7__LabTiming3";

    public static final String ID_READ_RECORD_5_LAB_4 = "PerformanceDevice_7__LabTiming4"; 

    public TestingPage(String rootFolder, String relativePath) {

        mRootFolder = rootFolder;
        mFileName = relativePath;

        buildMap(rootFolder + relativePath);
    
    }

    private Data extractPage(Document doc, String id, String title) {        
        Data data;
        switch(id) {
            default:
                data = new Data(id, title, extractValue(doc, id));
                break;
        }
        return data;
    }    

    private void buildMap(String absPath) {

        try {

            File input = new File(absPath);
            Document doc = Jsoup.parse(input, "UTF-8", "");

            mMap.put(ID_TESTING_SCOPE, extractPage(doc, ID_TESTING_SCOPE, TITLE_TESTING_SCOPE));

            mMap.put(ID_LAB_NAME, extractPage(doc, ID_LAB_NAME, TITLE_LAB_NAME));
        
            mMap.put(ID_TESTING_REQUIREMENT_COMMENTS, extractPage(doc, ID_TESTING_REQUIREMENT_COMMENTS, TITLE_TESTING_REQUIREMENT_COMMENTS));
        
            mMap.put(ID_REPORT_NUMBER, extractPage(doc, ID_REPORT_NUMBER, TITLE_REPORT_NUMBER));
        
            mMap.put(ID_TEST_REPORT_RECEIVED, extractPage(doc, ID_TEST_REPORT_RECEIVED, TITLE_TEST_REPORT_RECEIVED));
        
            mMap.put(ID_LAB_TESTING_STARTED, extractPage(doc, ID_LAB_TESTING_STARTED, TITLE_LAB_TESTING_STARTED));
        
            mMap.put(ID_LAB_TESTING_FINISHED, extractPage(doc, ID_LAB_TESTING_FINISHED, TITLE_LAB_TESTING_FINISHED));
        
            mMap.put(ID_TESTER, extractPage(doc, ID_TESTER, TITLE_TESTER));
        
            mMap.put(ID_TEST_SHEET, extractPage(doc, ID_TEST_SHEET, TITLE_TEST_SHEET));
        
            mMap.put(ID_CROSS_TESTING_STARTED, extractPage(doc, ID_CROSS_TESTING_STARTED, TITLE_CROSS_TESTING_STARTED));
        
            mMap.put(ID_CROSS_TESTING_FINISHED, extractPage(doc, ID_CROSS_TESTING_FINISHED, TITLE_CROSS_TESTING_FINISHED));
        
            mMap.put(ID_CROSS_TESTING_COMMENTS, extractPage(doc, ID_CROSS_TESTING_COMMENTS, TITLE_CROSS_TESTING_COMMENTS));
        
            mMap.put(ID_SHEET_CREATED, extractPage(doc, ID_SHEET_CREATED, TITLE_SHEET_CREATED));
        
            mMap.put(ID_PERFORMANCE_AVG_LAB, extractPage(doc, ID_PERFORMANCE_AVG_LAB, TITLE_PERFORMANCE_AVG_LAB));
        
            mMap.put(ID_SELECT_PPSE_LAB_1, extractPage(doc, ID_SELECT_PPSE_LAB_1, TITLE_SELECT_PPSE_LAB_1));
        
            mMap.put(ID_SELECT_PPSE_LAB_2, extractPage(doc, ID_SELECT_PPSE_LAB_2, TITLE_SELECT_PPSE_LAB_2));
        
            mMap.put(ID_SELECT_PPSE_LAB_3, extractPage(doc, ID_SELECT_PPSE_LAB_3, TITLE_SELECT_PPSE_LAB_3));
        
            mMap.put(ID_SELECT_PPSE_LAB_4, extractPage(doc, ID_SELECT_PPSE_LAB_4, TITLE_SELECT_PPSE_LAB_4));
        
            mMap.put(ID_SELECT_ADF_LAB_1, extractPage(doc, ID_SELECT_ADF_LAB_1, TITLE_SELECT_ADF_LAB_1));
        
            mMap.put(ID_SELECT_ADF_LAB_2, extractPage(doc, ID_SELECT_ADF_LAB_2, TITLE_SELECT_ADF_LAB_2));
        
            mMap.put(ID_SELECT_ADF_LAB_3, extractPage(doc, ID_SELECT_ADF_LAB_3, TITLE_SELECT_ADF_LAB_3));
            
            mMap.put(ID_SELECT_ADF_LAB_4, extractPage(doc, ID_SELECT_ADF_LAB_4, TITLE_SELECT_ADF_LAB_4));
        
            mMap.put(ID_GPO_LAB_1, extractPage(doc, ID_GPO_LAB_1, TITLE_GPO_LAB_1));
        
            mMap.put(ID_GPO_LAB_2, extractPage(doc, ID_GPO_LAB_2, TITLE_GPO_LAB_2));
        
            mMap.put(ID_GPO_LAB_3, extractPage(doc, ID_GPO_LAB_3, TITLE_GPO_LAB_3));
        
            mMap.put(ID_GPO_LAB_4, extractPage(doc, ID_GPO_LAB_4, TITLE_GPO_LAB_4));
        
            mMap.put(ID_READ_RECORD_1_LAB_1, extractPage(doc, ID_READ_RECORD_1_LAB_1, TITLE_READ_RECORD_1_LAB_1));
        
            mMap.put(ID_READ_RECORD_1_LAB_2, extractPage(doc, ID_READ_RECORD_1_LAB_2, TITLE_READ_RECORD_1_LAB_2));
        
            mMap.put(ID_READ_RECORD_1_LAB_3, extractPage(doc, ID_READ_RECORD_1_LAB_3, TITLE_READ_RECORD_1_LAB_3));
        
            mMap.put(ID_READ_RECORD_1_LAB_4, extractPage(doc, ID_READ_RECORD_1_LAB_4, TITLE_READ_RECORD_1_LAB_4));
        
            mMap.put(ID_READ_RECORD_2_LAB_1, extractPage(doc, ID_READ_RECORD_2_LAB_1, TITLE_READ_RECORD_2_LAB_1));
        
            mMap.put(ID_READ_RECORD_2_LAB_2, extractPage(doc, ID_READ_RECORD_2_LAB_2, TITLE_READ_RECORD_2_LAB_2));
        
            mMap.put(ID_READ_RECORD_2_LAB_3, extractPage(doc, ID_READ_RECORD_2_LAB_3, TITLE_READ_RECORD_2_LAB_3));
        
            mMap.put(ID_READ_RECORD_2_LAB_4, extractPage(doc, ID_READ_RECORD_2_LAB_4, TITLE_READ_RECORD_2_LAB_4));
        
            mMap.put(ID_READ_RECORD_3_LAB_1, extractPage(doc, ID_READ_RECORD_3_LAB_1, TITLE_READ_RECORD_3_LAB_1));
        
            mMap.put(ID_READ_RECORD_3_LAB_2, extractPage(doc, ID_READ_RECORD_3_LAB_2, TITLE_READ_RECORD_3_LAB_2));
        
            mMap.put(ID_READ_RECORD_3_LAB_3, extractPage(doc, ID_READ_RECORD_3_LAB_3, TITLE_READ_RECORD_3_LAB_3));
        
            mMap.put(ID_READ_RECORD_3_LAB_4, extractPage(doc, ID_READ_RECORD_3_LAB_4, TITLE_READ_RECORD_3_LAB_4));
        
            mMap.put(ID_READ_RECORD_4_LAB_1, extractPage(doc, ID_READ_RECORD_4_LAB_1, TITLE_READ_RECORD_4_LAB_1));
        
            mMap.put(ID_READ_RECORD_4_LAB_2, extractPage(doc, ID_READ_RECORD_4_LAB_2, TITLE_READ_RECORD_4_LAB_2));
        
            mMap.put(ID_READ_RECORD_4_LAB_3, extractPage(doc, ID_READ_RECORD_4_LAB_3, TITLE_READ_RECORD_4_LAB_3));
        
            mMap.put(ID_READ_RECORD_4_LAB_4, extractPage(doc, ID_READ_RECORD_4_LAB_4, TITLE_READ_RECORD_4_LAB_4));
        
            mMap.put(ID_READ_RECORD_5_LAB_1, extractPage(doc, ID_READ_RECORD_5_LAB_1, TITLE_READ_RECORD_5_LAB_1));
        
            mMap.put(ID_READ_RECORD_5_LAB_2, extractPage(doc, ID_READ_RECORD_5_LAB_2, TITLE_READ_RECORD_5_LAB_2));
        
            mMap.put(ID_READ_RECORD_5_LAB_3, extractPage(doc, ID_READ_RECORD_5_LAB_3, TITLE_READ_RECORD_5_LAB_3));
        
            mMap.put(ID_READ_RECORD_5_LAB_4, extractPage(doc, ID_READ_RECORD_5_LAB_4, TITLE_READ_RECORD_5_LAB_4));
 
            Elements elements = doc.getAllElements();
            for (Element element : elements) {
                if (element.id().equals("") == false) {
                    Logger.debug("element", "element_id = " + element.id());
                    mSTMap.put(element.id(), element.id());
                }
            }

        } catch(Exception exception) {
            Logger.warning("!!!!Exception : " + exception.getMessage());
        }
    }       

}