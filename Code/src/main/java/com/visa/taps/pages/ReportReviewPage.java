/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps.pages;

import java.io.File;

import com.visa.taps.Logger;
import com.visa.taps.data.Data;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ReportReviewPage extends HtmlPage {

    public final static String FILE_FILTER = "Interoperability - Report Review.html";

    public static final String TITLE_TESTING_SCOPE = "Testing Scope";

    public static final String TITLE_LAB_NAME = "Lab";

    public static final String TITLE_TESTING_REQUIREMENT_COMMENTS = "Testing Requirement Comments";

    public static final String TITLE_REPORT_NUMBER = "Report Number";

    public static final String TITLE_TEST_REPORT_RECEIVED = "Test Report Received";

    public static final String TITLE_LAB_TESTING_STARTED = "Lab Testing Started";

    public static final String TITLE_LAB_TESTING_FINISHED = "Lab Testing Finished";

    public static final String TITLE_REPORT_REVIEWER = "Report Reviewer";

    public static final String TITLE_REVIEWER_ASSIGNED = "Reviewer Assigned";

    public static final String TITLE_REPORT_REVIEW_STARTED = "Report Review Started";

    public static final String TITLE_ACTUAL_SHEET_SIZE = "Actual Sheet Size";

    public static final String TITLE_NOT_TESTED = "Not Tested";

    public static final String TITLE_NUMBER_TESTED = "Number Tested";

    public static final String TITLE_WEIGHTED_SCORE = "Weighted Score";

    public static final String TITLE_PASS = "Pass";

    public static final String TITLE_PASS_PERCENTAGE = "Pass Percentage";

    public static final String TITLE_FAIL = "Fail";

    public static final String TITLE_FAIL_PERCENTAGE = "Fail Percentage";

    public static final String TITLE_DISTANCE_FAILURES = "DF";

    public static final String TITLE_COMMUNICATIONS_FAILURES = "CF";

    public static final String TITLE_TRANSACTION_FAILURES = "TF";

    public static final String TITLE_OVERALL_RESULT = "Overall Result";

    public static final String TITLE_REPORT_REVIEW_FINISHED = "Review Finished";

    public static final String TITLE_REPORT_REVIEWER_COMMENTS = "Report Reviewer Comments";

    public static final String TITLE_ISSUES = "Issues";    

    public static final String ID_TESTING_SCOPE = "TestingRequirement_CategoryScopeId";

    public static final String ID_LAB_NAME = "TestingRequirement_LabId";

    public static final String ID_TESTING_REQUIREMENT_COMMENTS = "TestingRequirement_Comments";

    public static final String ID_REPORT_NUMBER = "LabReport_ReportNumber";

    public static final String ID_TEST_REPORT_RECEIVED = "LabReport_ReportReceivedDate";

    public static final String ID_LAB_TESTING_STARTED = "LabReport_LabStartDate";

    public static final String ID_LAB_TESTING_FINISHED = "LabReport_LabEndDate";

    public static final String ID_REPORT_REVIEWER = "ReportReview_Reviewer";

    public static final String ID_REVIEWER_ASSIGNED = "ReportReview_ReviewerAssignedDate";

    public static final String ID_REPORT_REVIEW_STARTED = "ReportReview_ReportReviewStartDate";

    public static final String ID_ACTUAL_SHEET_SIZE = "ReportReviewAdditionalData_ActualSheetSize";

    public static final String ID_NOT_TESTED = "ReportReviewAdditionalData_NotTested";

    public static final String ID_NUMBER_TESTED = "ReportReviewAdditionalData_NumberTested";

    public static final String ID_WEIGHTED_SCORE = "ReportReview_WeightedScore";

    public static final String ID_PASS = "ReportReviewAdditionalData_Pass";

    public static final String ID_PASS_PERCENTAGE = "ReportReviewAdditionalData_PassPercentage";

    public static final String ID_FAIL = "ReportReviewAdditionalData_Fail";

    public static final String ID_FAIL_PERCENTAGE = "ReportReviewAdditionalData_FailPercentage";

    public static final String ID_DISTANCE_FAILURES = "ReportReviewAdditionalData_DF";

    public static final String ID_COMMUNICATIONS_FAILURES = "ReportReviewAdditionalData_CF";

    public static final String ID_TRANSACTION_FAILURES = "ReportReviewAdditionalData_TF";

    public static final String ID_OVERALL_RESULT = "ReportReview_OverallResult";

    public static final String ID_REPORT_REVIEW_FINISHED = "ReportReview_ReportReviewEndDate";

    public static final String ID_REPORT_REVIEWER_COMMENTS = "ReportReview_ReportReviewComments";

    public static final String ID_ISSUES = "ReportReview_Issues";

    public ReportReviewPage(String rootFolder, String relativePath) {

        mRootFolder = rootFolder;
        mFileName = relativePath;

        buildMap(rootFolder + relativePath);
    
    }

    private Data extractPage(Document doc, String id, String title) {        
        Data data;
        switch(id) {
            default:
                data = new Data(id, title, extractValue(doc, id));
                break;
        }
        return data;
    }

    private void buildMap(String absPath) {

        try {

            File input = new File(absPath);
            Document doc = Jsoup.parse(input, "UTF-8", "");

            mMap.put(ID_TESTING_SCOPE, extractPage(doc, ID_TESTING_SCOPE, TITLE_TESTING_SCOPE));

            mMap.put(ID_LAB_NAME, extractPage(doc, ID_LAB_NAME, TITLE_LAB_NAME));
        
            mMap.put(ID_TESTING_REQUIREMENT_COMMENTS, extractPage(doc, ID_TESTING_REQUIREMENT_COMMENTS, TITLE_TESTING_REQUIREMENT_COMMENTS));
        
            mMap.put(ID_REPORT_NUMBER, extractPage(doc, ID_REPORT_NUMBER, TITLE_REPORT_NUMBER));
        
            mMap.put(ID_TEST_REPORT_RECEIVED, extractPage(doc, ID_TEST_REPORT_RECEIVED, TITLE_TEST_REPORT_RECEIVED));
        
            mMap.put(ID_LAB_TESTING_STARTED, extractPage(doc, ID_LAB_TESTING_STARTED, TITLE_LAB_TESTING_STARTED));
        
            mMap.put(ID_LAB_TESTING_FINISHED, extractPage(doc, ID_LAB_TESTING_FINISHED, TITLE_LAB_TESTING_FINISHED));
        
            mMap.put(ID_REPORT_REVIEWER, extractPage(doc, ID_REPORT_REVIEWER, TITLE_REPORT_REVIEWER));
        
            mMap.put(ID_REVIEWER_ASSIGNED, extractPage(doc, ID_REVIEWER_ASSIGNED, TITLE_REVIEWER_ASSIGNED));
        
            mMap.put(ID_REPORT_REVIEW_STARTED, extractPage(doc, ID_REPORT_REVIEW_STARTED, TITLE_REPORT_REVIEW_STARTED));
        
            mMap.put(ID_ACTUAL_SHEET_SIZE, extractPage(doc, ID_ACTUAL_SHEET_SIZE, TITLE_ACTUAL_SHEET_SIZE));
        
            mMap.put(ID_NOT_TESTED, extractPage(doc, ID_NOT_TESTED, TITLE_NOT_TESTED));
        
            mMap.put(ID_NUMBER_TESTED, extractPage(doc, ID_NUMBER_TESTED, TITLE_NUMBER_TESTED));
        
            mMap.put(ID_WEIGHTED_SCORE, extractPage(doc, ID_WEIGHTED_SCORE, TITLE_WEIGHTED_SCORE));
        
            mMap.put(ID_PASS, extractPage(doc, ID_PASS, TITLE_PASS));
        
            mMap.put(ID_PASS_PERCENTAGE, extractPage(doc, ID_PASS_PERCENTAGE, TITLE_PASS_PERCENTAGE));
        
            mMap.put(ID_FAIL, extractPage(doc, ID_FAIL, TITLE_FAIL));
        
            mMap.put(ID_FAIL_PERCENTAGE, extractPage(doc, ID_FAIL_PERCENTAGE, TITLE_FAIL_PERCENTAGE));
        
            mMap.put(ID_DISTANCE_FAILURES, extractPage(doc, ID_DISTANCE_FAILURES, TITLE_DISTANCE_FAILURES));
        
            mMap.put(ID_COMMUNICATIONS_FAILURES, extractPage(doc, ID_COMMUNICATIONS_FAILURES, TITLE_COMMUNICATIONS_FAILURES));
        
            mMap.put(ID_TRANSACTION_FAILURES, extractPage(doc, ID_TRANSACTION_FAILURES, TITLE_TRANSACTION_FAILURES));
        
            mMap.put(ID_OVERALL_RESULT, extractPage(doc, ID_OVERALL_RESULT, TITLE_OVERALL_RESULT));
        
            mMap.put(ID_REPORT_REVIEW_FINISHED, extractPage(doc, ID_REPORT_REVIEW_FINISHED, TITLE_REPORT_REVIEW_FINISHED));
        
            mMap.put(ID_REPORT_REVIEWER_COMMENTS, extractPage(doc, ID_REPORT_REVIEWER_COMMENTS, TITLE_REPORT_REVIEWER_COMMENTS));
        
            mMap.put(ID_ISSUES, extractPage(doc, ID_ISSUES, TITLE_ISSUES));
 
            Elements elements = doc.getAllElements();
            for (Element element : elements) {
                if (element.id().equals("") == false) {
                    Logger.debug("element", "element_id = " + element.id());
                    mSTMap.put(element.id(), element.id());
                }
            }

        } catch(Exception exception) {
            Logger.warning("!!!!Exception : " + exception.getMessage());
        }
    }    

}