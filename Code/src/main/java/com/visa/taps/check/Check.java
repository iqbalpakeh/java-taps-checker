/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps.check;

import java.util.ArrayList;

public class Check {

    private String mCheckTitle;

    private String mCheckDescription;

    private ArrayList<Report> mReportList;

    public Check() {
        mCheckTitle = "";
        mCheckDescription = "";
        mReportList = new ArrayList<>();
    }

    public void setTitle(String title) {
        mCheckTitle = title;
    }

    public String getTitle() {
        return mCheckTitle;
    }

    public void setDescription(String description) {
        mCheckDescription = description;
    }

    public String getDescription() {
        return mCheckDescription;
    }

    public void addReport(Report report) {
        mReportList.add(report);
    }

    public ArrayList<Report> getReportList() {
        return mReportList;
    }

}