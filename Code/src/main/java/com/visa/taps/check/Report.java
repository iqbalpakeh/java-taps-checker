/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/
package com.visa.taps.check;

public class Report {

    public static final String STATUS_PASS = "PASS";

    public static final String STATUS_ERROR = "ERROR";

    private String mDescription;

    private String mStatus;

    public Report() {
        mDescription = "";
        mStatus = "";
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getStatus() {
        return mStatus;
    }

 }