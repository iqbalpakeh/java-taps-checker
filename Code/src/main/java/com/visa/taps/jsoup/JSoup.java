/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps.jsoup;

import com.visa.taps.Logger;

import java.io.File;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class JSoup {

    public static void main(String[] args) {
        JSoup jSoup = new JSoup();
        jSoup.sampleGetObjectIds();
    }

    private void testElemet(Document doc, String id, String attr, String expected) {
        
        Element content = doc.getElementById(id);
        if (content.attr(attr).equals(expected)) {
            Logger.warning("PASS : " + id + " = " + content.val());
        } else {
            Logger.warning("ERROR : " + id);
            Logger.warning("  EXPECTED : " + expected);
            Logger.warning("  ACTUAL   : " + content.val());
        }
    }

    public void sampleGetVtfList() {

        try {

            File input = new File("/Users/mpakeh/Documents/Projects/TapsRegressionChecker_3.0/Resource/Resource/TAPS_3.9_SNAPSHOT_1/1__MSD_qVSDC_Interop/CDTSSS1063A/INTEROP/Interoperability - Results.html");
            Document doc = Jsoup.parse(input, "UTF-8", "");

            Elements tbodys = doc.getElementsByTag("tbody");

            for (Element tbody : tbodys) {

                Logger.warning("tbody = " + tbody.tagName());

                for (Element tr : tbody.children()) {

                    Logger.warning("tr = " + tr.tagName());

                    int i = 0;

                    for (Element td : tr.children()) {

                        Logger.warning("td = " + td.tagName());
                        Logger.warning("td_value = " + td.html());
                        Logger.warning("td_order = " + i++);

                    }

                    Logger.warning("\n");

                }

                Logger.warning("\n");                

            }

        } catch(Exception exception) {
            Logger.warning("!!!!Exception : " + exception.getMessage());
        }        

    }

    public void sampleGetObjectIds() {

        try {

            File input = new File("/Users/mpakeh/Documents/Projects/TapsRegressionChecker_3.0/Resource/Resource/TAPS_3.9_SNAPSHOT_1/1__MSD_qVSDC_Interop/CDTSSS1063A/INTEROP/Interoperability - Report Review.html");
            Document doc = Jsoup.parse(input, "UTF-8", "");

            Elements elements = doc.getAllElements();
            for (Element element : elements) {
                if (element.id().equals("") == false) {
                    Logger.warning("element_id = " + element.id());
                }
            }

        } catch(Exception exception) {
            Logger.warning("!!!!Exception : " + exception.getMessage());
        }

    }

    public void sampleReadObject() {

        String id = "";
        String attr = "";
        String expected = "";

        try {

            File input = new File("/Users/mpakeh/Documents/Projects/TapsRegressionChecker_3.0/Resource/Resource/TAPS_3.9_SNAPSHOT_1/1__MSD_qVSDC_Interop/CDDPSL1133A/MSD/MSD.html");
            Document doc = Jsoup.parse(input, "UTF-8", "");

            id = "TestingRequirement_CategoryScopeId";
            attr = "value"; 
            expected = "Full";
            testElemet(doc, id, attr, expected);

            id = "TestingRequirement_LabId";
            attr = "value"; 
            expected = "Underwriters Laboratories";
            testElemet(doc, id, attr, expected);

            id = "TestingRequirement_Comments";
            attr = "value"; 
            expected = "";
            testElemet(doc, id, attr, expected);            

            id = "LabReport_ReportNumber";
            attr = "value"; 
            expected = "11770729";
            testElemet(doc, id, attr, expected);                       

            id = "LabReport_TestPlanVersion";
            attr = "value"; 
            expected = "2.1.3b";
            testElemet(doc, id, attr, expected);              

            id = "LabReport_LabStartDate";
            attr = "value"; 
            expected = "19 Jun 2017";
            testElemet(doc, id, attr, expected);             

            id = "LabReport_LabEndDate";
            attr = "value"; 
            expected = "13 Jul 2017";
            testElemet(doc, id, attr, expected);   
            
            // Special case for textarea
            id = "ReportReview_ReportReviewHistory";
            attr = "value"; 
            expected = "xxx";
            Elements elements_ReportReview_ReportReviewHistory = doc.select("textarea[id^=ReportReview_ReportReviewHistory]");
            for(Element e : elements_ReportReview_ReportReviewHistory) {
                System.out.println(e.html().replace("\n", "").replace(" ", "").trim());
            }

            id = "ReportReview_ReportReviewer";
            attr = "value"; 
            expected = "Jeremy Loh";
            testElemet(doc, id, attr, expected);  

            id = "ReportReview_ReviewerAssignedDate";
            attr = "value"; 
            expected = "18 Aug 2017";
            testElemet(doc, id, attr, expected);  

            id = "LabReport_ReportReceivedDate";
            attr = "value"; 
            expected = "31 Jul 2017";
            testElemet(doc, id, attr, expected);      
            
            id = "ReportReview_ReportReviewStartDate";
            attr = "value"; 
            expected = "18 Aug 2017";
            testElemet(doc, id, attr, expected);

            id = "ReportReview_IssuesCount";
            attr = "value"; 
            expected = "1";
            testElemet(doc, id, attr, expected);           
            
            // Special case for textarea
            id = "ReportReview_IssuesDescription";
            attr = "value"; 
            Elements elements_ReportReview_IssuesDescription = doc.select("textarea[id^=ReportReview_IssuesDescription]");
            for(Element e : elements_ReportReview_IssuesDescription) {
                System.out.println(e.html().replace("\n", "").replace(" ", "").trim());
            }

            // Special case for textarea
            id = "ReportReview_ReviewerComment";
            attr = "value"; 
            Elements elements_ReportReview_ReviewerComment = doc.select("textarea[id^=ReportReview_ReviewerComment]");
            for(Element e : elements_ReportReview_ReviewerComment) {
                System.out.println(e.html().replace("\n", "").replace(" ", "").trim());
            }            

            id = "ReportReview_ReviewerManager";
            attr = "value"; 
            expected = "Karen Siew Kuen Lee";
            testElemet(doc, id, attr, expected); 
            
            id = "ReportReview_ReportReviewEndDate";
            attr = "value"; 
            expected = "18 Aug 2017";
            testElemet(doc, id, attr, expected);             

            id = "ReportReview_OverallResult";
            attr = "value"; 
            expected = "Pass with Comments";
            testElemet(doc, id, attr, expected);

            id = "ReportReview_ReviewCompletedDate";
            attr = "value"; 
            expected = "18 Aug 2017";
            testElemet(doc, id, attr, expected);            

        } catch(Exception exception) {
            Logger.warning("!!!!Exception : " + exception.getMessage());
        }
    }

}