/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import com.visa.taps.check.Check;
import com.visa.taps.check.Report;
import com.visa.taps.pages.HtmlPage;
import com.visa.taps.pages.MSDPage;
import com.visa.taps.pages.PreTestPage;
import com.visa.taps.pages.QVSDCPage;
import com.visa.taps.pages.ReportReviewPage;
import com.visa.taps.pages.TestingPage;
import com.visa.taps.pages.XTPage;
import com.visa.taps.data.Data;
import com.visa.taps.data.XTData;

public class Parser {

    /**
     * User parameter to show pass comparison for more detail analysis
     */
    private boolean mShowPass;

    /**
     * List of check object for every test scenario
     */
    private ArrayList<Check> mChecks;

    /**
     * File list of old TAPS version
     */
    private ArrayList<String> mOldFileList;

    /**
     * File list of new TAPS version
     */
    private ArrayList<String> mNewFileList;

    /**
     * List of MSDPage for every MSD.html files from old snapshot
     */
    private ArrayList<MSDPage> mOldMSDPages;    

    /**
     * List of MSDPage for every MSD.html files from new snapshot
     */
    private ArrayList<MSDPage> mNewMSDPages;    

    /**
     * List of QVSDCPage for every qVSDC.html files from old snapshot
     */    
    private ArrayList<QVSDCPage> mOldQVSDCPages;

    /**
     * List of QVSDCPage for every qVSDC.html files from new snapshot
     */    
    private ArrayList<QVSDCPage> mNewQVSDCPages;    

    /**
     * List of PreTest for every Interoperability - Pre Test.html files from old snapshot
     */    
    private ArrayList<PreTestPage> mOldPreTestPages;

    /**
     * List of PreTest for every Interoperability - Pre Test.html files from new snapshot
     */    
    private ArrayList<PreTestPage> mNewPreTestPages;       

    /**
     * List of PreTest for every Interoperability - Report Review.html files from old snapshot
     */    
    private ArrayList<ReportReviewPage> mOldReportReviewPages;

    /**
     * List of PreTest for every Interoperability - Report Review.html files from new snapshot
     */    
    private ArrayList<ReportReviewPage> mNewReportReviewPages;

    /**
     * List of PreTest for every Interoperability - Testing.html files from old snapshot
     */       
    private ArrayList<TestingPage> mOldTestingPages;

    /**
     * List of PreTest for every Interoperability - Testing.html files from new snapshot
     */ 
    private ArrayList<TestingPage> mNewTestingPages;

    /**
     * List of PreTest for every Interoperability - Results.htm files from old snapshot
     */       
    private ArrayList<XTPage> mOldXTPages;

    /**
     * List of PreTest for every Interoperability - Results.htm files from new snapshot
     */ 
    private ArrayList<XTPage> mNewXTPages;    

    /**
     * Content path location of folder containing older version of html snapshot
     */
    private String mOldPathRoot;

    /**
     * Content path location of folder containing older version of html snapshot
     */    
    private String mNewPathRoot;

    /**
     * Main constructor
     * 
     * @param show pass comparison for more detail analysis
     * @param oldPathRoot contain old html path location
     * @param newdPathRoot contain new html path location
     */
    public Parser(String show, String oldPathRoot, String newPathRoot) {
        
        if (show.equals("show")) {
            mShowPass = true;
        } else if (show.equals("hide")) {
            mShowPass = false;
        } else {
            Logger.warning("Input error, follow template below:");
            Logger.warning("$ java -jar tapschecker.jar [show or hide] [old_root] [new_root]");
            System.exit(0);
        }
            
        mOldPathRoot = oldPathRoot;
        mNewPathRoot = newPathRoot;
        Logger.debug("path", mOldPathRoot);
        Logger.debug("path", mNewPathRoot);
        
        mChecks = new ArrayList<>();
    }

    /**
     * Fetch all necessary data from both old and new path. Then store all the data
     * object to be checked during check state. 
     */
    public void fetch() {
        fetchFileList();
        fetchMSDPages();
        fetchQVSDCPage();
        fetchPreTestPage();
        fetchReportReviewPage();
        fetchTestingPage();
        fetchXTPage();
    }

    /**
     * Check all the object after being fetched during fetch state. Store the result 
     * to the report list so that it will be reported during report check.
     */
    public void check() {
        checkFileList();
        checkMSDPages();
        checkQVSDCPages();
        checkPreTestPages();
        checkReportReviewPages();
        checkTestingPages();
        checkXTPages();
    }    

    /**
     * Fetch Cross Test pages and extract the important content
     */     
    private void fetchXTPage() {

        mOldXTPages = new ArrayList<>();
        mNewXTPages = new ArrayList<>();

        for (String file : mOldFileList) {
            if (file.endsWith(XTPage.FILE_FILTER)) {
                mOldXTPages.add(new XTPage(mOldPathRoot, file));
            }
        }

        for (String file : mNewFileList) {
            if (file.endsWith(XTPage.FILE_FILTER)) {
                mNewXTPages.add(new XTPage(mNewPathRoot, file));
            }
        }  

    }

    /**
     * Fetch Testing pages and extract the important content
     */     
    private void fetchTestingPage() {

        mOldTestingPages = new ArrayList<>();
        mNewTestingPages = new ArrayList<>();

        for (String file : mOldFileList) {
            if (file.endsWith(TestingPage.FILE_FILTER)) {
                mOldTestingPages.add(new TestingPage(mOldPathRoot, file));
            }
        }

        for (String file : mNewFileList) {
            if (file.endsWith(TestingPage.FILE_FILTER)) {
                mNewTestingPages.add(new TestingPage(mNewPathRoot, file));
            }
        } 

    }


    /**
     * Fetch Report Review pages and extract the important content
     */ 
    private void fetchReportReviewPage() {

        mOldReportReviewPages = new ArrayList<>();
        mNewReportReviewPages = new ArrayList<>();

        for (String file : mOldFileList) {
            if (file.endsWith(ReportReviewPage.FILE_FILTER)) {
                mOldReportReviewPages.add(new ReportReviewPage(mOldPathRoot, file));
            }
        }

        for (String file : mNewFileList) {
            if (file.endsWith(ReportReviewPage.FILE_FILTER)) {
                mNewReportReviewPages.add(new ReportReviewPage(mNewPathRoot, file));
            }
        } 
    }

    /**
     * Fetch PreTest pages and extract the important content
     */    
    private void fetchPreTestPage() {

        mOldPreTestPages = new ArrayList<>();
        mNewPreTestPages = new ArrayList<>();

        for (String file : mOldFileList) {
            if (file.endsWith(PreTestPage.FILE_FILTER)) {
                mOldPreTestPages.add(new PreTestPage(mOldPathRoot, file));
            }
        }

        for (String file : mNewFileList) {
            if (file.endsWith(PreTestPage.FILE_FILTER)) {
                mNewPreTestPages.add(new PreTestPage(mNewPathRoot, file));
            }
        } 

    }

    /**
     * Fetch QVSDC pages and extract the important content
     */    
    private void fetchQVSDCPage() {

        mOldQVSDCPages = new ArrayList<>();
        mNewQVSDCPages = new ArrayList<>();

        for (String file : mOldFileList) {
            if (file.endsWith(QVSDCPage.FILE_FILTER)) {
                mOldQVSDCPages.add(new QVSDCPage(mOldPathRoot, file));
            }
        }

        for (String file : mNewFileList) {
            if (file.endsWith(QVSDCPage.FILE_FILTER)) {
                mNewQVSDCPages.add(new QVSDCPage(mNewPathRoot, file));
            }
        }        
    }

    /**
     * Fetch MSD pages and extract the important content
     */
    private void fetchMSDPages() {

        mOldMSDPages = new ArrayList<>();
        mNewMSDPages = new ArrayList<>();

        for (String file : mOldFileList) {
            if (file.endsWith(MSDPage.FILE_FILTER)) {
                mOldMSDPages.add(new MSDPage(mOldPathRoot, file));
            }
        }

        for (String file : mNewFileList) {
            if (file.endsWith(MSDPage.FILE_FILTER)) {
                mNewMSDPages.add(new MSDPage(mNewPathRoot, file));
            }
        }
    }

    /**
     * Fetch file list from both old and new snapshot
     */
    private void fetchFileList() {

        mOldFileList = new ArrayList<>();
        mNewFileList = new ArrayList<>();

        walk(new File(mOldPathRoot), "", mOldFileList);
        walk(new File(mNewPathRoot), "", mNewFileList);

        for (String fileName : mOldFileList) {
            Logger.debug("file_list", "Old :" + fileName);
        }

        for (String fileName : mNewFileList) {
            Logger.debug("file_list", "New : " + fileName);
        }
    }

    /**
     * Check all Cross Testing pages consistency
     */    
    private void checkXTPages() {

        Check check = new Check();
        check.setTitle("Test " + XTPage.FILE_FILTER + " pages consistency");
        check.setDescription("Check ALL INFORMATION in Interoperability - Results.html pages from both old and new pages");

        for (XTPage oldPage : mOldXTPages) {            
            for (XTPage newPage : mNewXTPages) {

                if (oldPage.getFileName().equals(newPage.getFileName())) {

                    { // Data consistency check

                        Logger.debug("xt_it", "file name = " + oldPage.getFullPath());

                        // Checking based on oldpage iterator
                        Iterator<Map.Entry<String, XTData>> oldIterator =  oldPage.getXTMap().entrySet().iterator();
                        while(oldIterator.hasNext()) {
    
                            Map.Entry<String, XTData> entry = oldIterator.next();
                            Logger.debug("xt_it", "key = " + entry.getKey());
                            
                            XTData oldXTData = entry.getValue();
                            XTData newXTData = newPage.getXTDataByKey(entry.getKey());
    
                            if (newXTData != null) {
    
                                Report report = new Report();
                                report.setStatus(Report.STATUS_PASS);
                                report.setDescription(
                                        "VTF " + oldXTData.getVTF() + " exist in " + newPage.getFullPath() + 
                                        "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                        "\n" + "NEW HTML :> " + newPage.getFullPath());
                                check.addReport(report);
    
                                // Check Verdict:
                                if(oldXTData.getVerdict().equals(newXTData.getVerdict())) {
    
                                    Report reportAttribute = new Report();
                                    reportAttribute.setStatus(Report.STATUS_PASS);
                                    reportAttribute.setDescription(
                                            "VTF " + oldXTData.getVTF() + " has similar Verdict :> " + oldXTData.getVerdict() + 
                                            "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                            "\n" + "NEW HTML :> " + newPage.getFullPath());
                                    check.addReport(reportAttribute);
    
                                } else {
    
                                    Report reportAttribute = new Report();
                                    reportAttribute.setStatus(Report.STATUS_ERROR);
                                    reportAttribute.setDescription(
                                            "VTF " + oldXTData.getVTF() + " has different Verdict " +
                                            "\n" + "OLD VERDICT :> " + oldXTData.getVerdict() + 
                                            "\n" + "NEW VERDICT :> " + newXTData.getVerdict() + 
                                            "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                            "\n" + "NEW HTML :> " + newPage.getFullPath());
                                    check.addReport(reportAttribute);
    
                                }
    
                                // Check Lab:
                                if(oldXTData.getLab().equals(newXTData.getLab())) {
    
                                    Report reportAttribute = new Report();
                                    reportAttribute.setStatus(Report.STATUS_PASS);
                                    reportAttribute.setDescription(
                                            "VTF " + oldXTData.getVTF() + " has similar Lab :> " + oldXTData.getLab() + 
                                            "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                            "\n" + "NEW HTML :> " + newPage.getFullPath());
                                    check.addReport(reportAttribute);
    
                                } else {
    
                                    Report reportAttribute = new Report();
                                    reportAttribute.setStatus(Report.STATUS_ERROR);
                                    reportAttribute.setDescription(
                                            "VTF " + oldXTData.getVTF() + " has different Lab " +
                                            "\n" + "OLD LAB :> " + oldXTData.getLab() + 
                                            "\n" + "NEW LAB :> " + newXTData.getLab() + 
                                            "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                            "\n" + "NEW HTML :> " + newPage.getFullPath());
                                    check.addReport(reportAttribute);
    
                                }      
                                
                                // Check Result:
                                if(oldXTData.getResult().equals(newXTData.getResult())) {
    
                                    Report reportAttribute = new Report();
                                    reportAttribute.setStatus(Report.STATUS_PASS);
                                    reportAttribute.setDescription(
                                            "VTF " + oldXTData.getVTF() + " has similar Result :> " + oldXTData.getResult() + 
                                            "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                            "\n" + "NEW HTML :> " + newPage.getFullPath());
                                    check.addReport(reportAttribute);
    
                                } else {
    
                                    Report reportAttribute = new Report();
                                    reportAttribute.setStatus(Report.STATUS_ERROR);
                                    reportAttribute.setDescription(
                                            "VTF " + oldXTData.getVTF() + " has different Result " +
                                            "\n" + "OLD RESULT :> " + oldXTData.getResult() + 
                                            "\n" + "NEW RESULT :> " + newXTData.getResult() + 
                                            "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                            "\n" + "NEW HTML :> " + newPage.getFullPath());
                                    check.addReport(reportAttribute);
    
                                }   
                                
                                // Check Correct:
                                if(oldXTData.getCorrect().equals(newXTData.getCorrect())) {
    
                                    Report reportAttribute = new Report();
                                    reportAttribute.setStatus(Report.STATUS_PASS);
                                    reportAttribute.setDescription(
                                            "VTF " + oldXTData.getVTF() + " has similar Correct :> " + oldXTData.getCorrect() + 
                                            "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                            "\n" + "NEW HTML :> " + newPage.getFullPath());
                                    check.addReport(reportAttribute);
    
                                } else {
    
                                    Report reportAttribute = new Report();
                                    reportAttribute.setStatus(Report.STATUS_ERROR);
                                    reportAttribute.setDescription(
                                            "VTF " + oldXTData.getVTF() + " has different Correct " +
                                            "\n" + "OLD CORRECT :> " + oldXTData.getCorrect() + 
                                            "\n" + "NEW CORRECT :> " + newXTData.getCorrect() + 
                                            "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                            "\n" + "NEW HTML :> " + newPage.getFullPath());
                                    check.addReport(reportAttribute);
    
                                }                             
    
                            } else {
    
                                Report report = new Report();
                                report.setStatus(Report.STATUS_ERROR);
                                report.setDescription(
                                        "VTF " + oldXTData.getVTF() + " does not exist in " + newPage.getFullPath() + 
                                        "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                        "\n" + "NEW HTML :> " + newPage.getFullPath());
                                check.addReport(report);
                            }
                            
                        }
    
                        // Checking based on newpage iterator
                        Iterator<Map.Entry<String, XTData>> newIterator =  newPage.getXTMap().entrySet().iterator();
                        while(newIterator.hasNext()) {
    
                            Map.Entry<String, XTData> entry = newIterator.next();
                            Logger.debug("xt_it", "key = " + entry.getKey());
                            
                            XTData newXTData = entry.getValue();
                            XTData oldXTData = oldPage.getXTDataByKey(entry.getKey());
    
                            if (oldXTData != null) {
    
                                Report report = new Report();
                                report.setStatus(Report.STATUS_PASS);
                                report.setDescription(
                                        "VTF " + newXTData.getVTF() + "exist in " + oldPage.getFullPath() + 
                                        "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                        "\n" + "NEW HTML :> " + newPage.getFullPath());
                                check.addReport(report);
    
                            } else {
    
                                Report report = new Report();
                                report.setStatus(Report.STATUS_ERROR);
                                report.setDescription(
                                        "VTF " + newXTData.getVTF() + "does not exist in " + oldPage.getFullPath() + 
                                        "\n" + "OLD HTML :> " + oldPage.getFullPath() +
                                        "\n" + "NEW HTML :> " + newPage.getFullPath());
                                check.addReport(report);
                            } 
                        }
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("xt_st", "file name = " + oldPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  oldPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {

                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("xt_st", "key = " + entry.getKey());
                            Logger.debug("xt_st", "value = " + entry.getValue());

                            if (entry.getKey().equals(XTPage.NUMBER_OF_COLUMNS)) {
                                check.addReport(compareTable(oldPage, newPage));
                            } else {
                                check.addReport(compareSTData(entry.getKey(), oldPage, newPage));
                            }                            
                        }
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("xt_st", "file name = " + newPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  newPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {

                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("xt_st", "key = " + entry.getKey());
                            Logger.debug("xt_st", "value = " + entry.getValue());
                            
                            if (entry.getKey().equals(XTPage.NUMBER_OF_COLUMNS)) {
                                check.addReport(compareTable(newPage, oldPage));
                            } else {
                                check.addReport(compareSTData(entry.getKey(), newPage, oldPage));
                            }                            
                        }
                    } 

                }
            }
        }
        mChecks.add(check);
    }

    /**
     * Check all Testing pages consistency
     */    
    private void checkTestingPages() {

        Check check = new Check();
        check.setTitle("Test " + TestingPage.FILE_FILTER + " consistency");
        check.setDescription("Check ALL INFORMATION in Report Review pages from both old and new pages");

        for (TestingPage oldPage : mOldTestingPages) {            
            for (TestingPage newPage : mNewTestingPages) {
                if (oldPage.getFileName().equals(newPage.getFileName())) {

                    { // Data consistency check

                        Logger.debug("tp_it", "file name = " + oldPage.getFullPath());
                        Iterator<Map.Entry<String, Data>> iterator =  oldPage.getMap().entrySet().iterator();                
                        while(iterator.hasNext()) {
                            Map.Entry<String, Data> entry = iterator.next();
                            Logger.debug("tp_it", "key = " + entry.getKey());
                            Logger.debug("tp_it", "id = " + entry.getValue().getId());
                            Logger.debug("tp_it", "title = " + entry.getValue().getTitle());
                            Logger.debug("tp_it", "value = " + entry.getValue().getValue() + "\n");
                            check.addReport(compareData(entry.getValue().getId(), oldPage, newPage));
                        }
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("tp_st", "file name = " + oldPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  oldPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {
                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("tp_st", "key = " + entry.getKey());
                            Logger.debug("tp_st", "value = " + entry.getValue());
                            check.addReport(compareSTData(entry.getKey(), oldPage, newPage));
                        }
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("tp_st", "file name = " + newPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  newPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {
                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("tp_st", "key = " + entry.getKey());
                            Logger.debug("tp_st", "value = " + entry.getValue());
                            check.addReport(compareSTData(entry.getKey(), newPage, oldPage));
                        }
                    }  
                }
            }
        }
        mChecks.add(check);

    }

    /**
     * Check all Report Review pages consistency
     */
    private void checkReportReviewPages() {

        Check check = new Check();
        check.setTitle("Test " + ReportReviewPage.FILE_FILTER + " pages consistency");
        check.setDescription("Check ALL INFORMATION in Report Review pages from both old and new pages");

        for (ReportReviewPage oldPage : mOldReportReviewPages) {            
            for (ReportReviewPage newPage : mNewReportReviewPages) {
                if (oldPage.getFileName().equals(newPage.getFileName())) {

                    { // Data consistency check

                        Logger.debug("rr_it", "file name = " + oldPage.getFullPath());
                        Iterator<Map.Entry<String, Data>> iterator =  oldPage.getMap().entrySet().iterator();                    
                        while(iterator.hasNext()) {
                            Map.Entry<String, Data> entry = iterator.next();
                            Logger.debug("rr_it", "key = " + entry.getKey());
                            Logger.debug("rr_it", "id = " + entry.getValue().getId());
                            Logger.debug("rr_it", "title = " + entry.getValue().getTitle());
                            Logger.debug("rr_it", "value = " + entry.getValue().getValue() + "\n");
                            check.addReport(compareData(entry.getValue().getId(), oldPage, newPage));
                        }
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("rr_st", "file name = " + oldPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  oldPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {
                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("rr_st", "key = " + entry.getKey());
                            Logger.debug("rr_st", "value = " + entry.getValue());
                            check.addReport(compareSTData(entry.getKey(), oldPage, newPage));
                        }
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("rr_st", "file name = " + newPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  newPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {
                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("rr_st", "key = " + entry.getKey());
                            Logger.debug("rr_st", "value = " + entry.getValue());
                            check.addReport(compareSTData(entry.getKey(), newPage, oldPage));
                        }
                    }  
                }
            }
        }
        mChecks.add(check); 
    }

    /**
     * Check all Pre Test pages consistency
     */
    private void checkPreTestPages() {

        Check check = new Check();
        check.setTitle("Test " + PreTestPage.FILE_FILTER + " pages consistency");
        check.setDescription("Check ALL INFORMATION in PreTest pages from both old and new pages");

        for (PreTestPage oldPage : mOldPreTestPages) {            
            for (PreTestPage newPage : mNewPreTestPages) {
                if (oldPage.getFileName().equals(newPage.getFileName())) {

                    { // Data consistency check

                        Logger.debug("pt_it", "file name = " + oldPage.getFullPath());
                        Iterator<Map.Entry<String, Data>> iterator =  oldPage.getMap().entrySet().iterator();                    
                        while(iterator.hasNext()) {
                            Map.Entry<String, Data> entry = iterator.next();
                            Logger.debug("pt_it", "key = " + entry.getKey());
                            Logger.debug("pt_it", "id = " + entry.getValue().getId());
                            Logger.debug("pt_it", "title = " + entry.getValue().getTitle());
                            Logger.debug("pt_it", "value = " + entry.getValue().getValue() + "\n");
                            check.addReport(compareData(entry.getValue().getId(), oldPage, newPage));
                        }
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("pt_st", "file name = " + oldPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  oldPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {
                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("pt_st", "key = " + entry.getKey());
                            Logger.debug("pt_st", "value = " + entry.getValue());
                            check.addReport(compareSTData(entry.getKey(), oldPage, newPage));
                        }
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("pt_st", "file name = " + newPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  newPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {
                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("pt_st", "key = " + entry.getKey());
                            Logger.debug("pt_st", "value = " + entry.getValue());
                            check.addReport(compareSTData(entry.getKey(), newPage, oldPage));
                        }
                    }                       
                }
            }
        }
        mChecks.add(check);      
    }

    /**
     * Check all QVSDC pages consistency
     */
    private void checkQVSDCPages() {

        Check check = new Check();
        check.setTitle("Test " + QVSDCPage.FILE_FILTER + " pages consistency");
        check.setDescription("Check ALL INFORMATION in QVSDC pages from both old and new pages");

        for (QVSDCPage oldPage : mOldQVSDCPages) {
            for (QVSDCPage newPage : mNewQVSDCPages) {
                if (oldPage.getFileName().equals(newPage.getFileName())) {

                    { // Data consistency check

                        Logger.debug("qvsdc_it", "file name = " + oldPage.getFullPath());
                        Iterator<Map.Entry<String, Data>> iterator =  oldPage.getMap().entrySet().iterator();
                        while(iterator.hasNext()) {
                            Map.Entry<String, Data> entry = iterator.next();
                            Logger.debug("qvsdc_it", "key = " + entry.getKey());
                            Logger.debug("qvsdc_it", "id = " + entry.getValue().getId());
                            Logger.debug("qvsdc_it", "title = " + entry.getValue().getTitle());
                            Logger.debug("qvsdc_it", "value = " + entry.getValue().getValue() + "\n");
                            check.addReport(compareData(entry.getValue().getId(), oldPage, newPage));
                        }
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("qvsdc_st", "file name = " + oldPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  oldPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {
                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("qvsdc_st", "key = " + entry.getKey());
                            Logger.debug("qvsdc_st", "value = " + entry.getValue());
                            check.addReport(compareSTData(entry.getKey(), oldPage, newPage));
                        }
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("qvsdc_st", "file name = " + newPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  newPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {
                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("qvsdc_st", "key = " + entry.getKey());
                            Logger.debug("qvsdc_st", "value = " + entry.getValue());
                            check.addReport(compareSTData(entry.getKey(), newPage, oldPage));
                        }
                    }                    
                }
            }
        }
        mChecks.add(check);            
    }

    /**
     * Check all MSD pages consistency
     */
    private void checkMSDPages() {

        Check check = new Check();
        check.setTitle("Test " + MSDPage.FILE_FILTER + " pages consistency");
        check.setDescription("Check ALL INFORMATION in MSD pages from both old and new pages");

        for (MSDPage oldPage : mOldMSDPages) {
            for (MSDPage newPage : mNewMSDPages) {
                if (oldPage.getFileName().equals(newPage.getFileName())) {
                    
                    { // Data consistency check   
                        
                        Logger.debug("msd_it", "file name = " + oldPage.getFullPath());
                        Iterator<Map.Entry<String, Data>> iterator =  oldPage.getMap().entrySet().iterator();    
                        while(iterator.hasNext()) {
                            Map.Entry<String, Data> entry = iterator.next();
                            Logger.debug("msd_it", "key = " + entry.getKey());
                            Logger.debug("msd_it", "id = " + entry.getValue().getId());
                            Logger.debug("msd_it", "title = " + entry.getValue().getTitle());
                            Logger.debug("msd_it", "value = " + entry.getValue().getValue() + "\n");
                            check.addReport(compareData(entry.getValue().getId(), oldPage, newPage));
                        }
    
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("msd_st", "file name = " + oldPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  oldPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {
                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("msd_st", "key = " + entry.getKey());
                            Logger.debug("msd_st", "value = " + entry.getValue());
                            check.addReport(compareSTData(entry.getKey(), oldPage, newPage));
                        }
                    }

                    { // Html structure consistency check
                        
                        Logger.debug("msd_st", "file name = " + newPage.getFullPath());
                        Iterator<Map.Entry<String, String>> iterator =  newPage.getSTMap().entrySet().iterator();   
                        while(iterator.hasNext()) {
                            Map.Entry<String, String> entry = iterator.next();
                            Logger.debug("msd_st", "key = " + entry.getKey());
                            Logger.debug("msd_st", "value = " + entry.getValue());
                            check.addReport(compareSTData(entry.getKey(), newPage, oldPage));
                        }
                    }
                }
            }
        }
        mChecks.add(check);        
    }

    /**
     * Compare number of column from XT table
     * 
     * @param key of observed object 
     * @param sourcePage of XT html 
     * @param referencePage of XT html
     */      
    private Report compareTable(XTPage sourcePage, XTPage referencePage) {
        
        Report report = new Report();
        String sourceValue = sourcePage.getSTDataByKey(XTPage.NUMBER_OF_COLUMNS);
        String referenceValue = referencePage.getSTDataByKey(XTPage.NUMBER_OF_COLUMNS);

        if (sourceValue.equals(referenceValue)) {
            // pass
            report.setStatus(Report.STATUS_PASS);
            report.setDescription("NUMBER OF COLUMNS (MAY CONTAIN META-ELEMENT) ARE SIMILAR ON BOTH HTML VERSION" +
                    "\n" + "VALUE          :> " + sourceValue + 
                    "\n" + "SOURCE HTML    :> " + sourcePage.getFullPath() + 
                    "\n" + "REFERENCE HTML :> " + referencePage.getFullPath()); 
        } else {
            // failed
            report.setStatus(Report.STATUS_ERROR);
            report.setDescription("NUMBER OF COLUMNS (MAY CONTAIN META-ELEMENT) ARE DIFFERENT ON BOTH HTML VERSION" + 
                    "\n" + "SOURCE VALUE    :> " + sourceValue + 
                    "\n" + "REFERENCE VALUE :> " + referenceValue + 
                    "\n" + "SOURCE HTML     :> " + sourcePage.getFullPath() + 
                    "\n" + "REFERENCE HTML  :> " + referencePage.getFullPath());            
        }           

        return report;  

    }

    /**
     * Compare HTML Structure Data (Identifyable Html Element Tag) from Source and Reference snapshot
     * 
     * @param key of observed object 
     * @param sourcePage of html 
     * @param referencePage of html
     */    
    private Report compareSTData(String key, HtmlPage sourcePage, HtmlPage referencePage) {

        Report report = new Report();
        String sourceValue = sourcePage.getSTDataByKey(key);
        String referenceValue = referencePage.getSTDataByKey(key);

        if (sourceValue.equals(referenceValue)) {
            // pass
            report.setStatus(Report.STATUS_PASS);
            report.setDescription("ELEMENT ID " + key + " EXIST ON BOTH HTML VERSION" +
                    "\n" + "SOURCE HTML    :> " + sourcePage.getFullPath() + 
                    "\n" + "REFERENCE HTML :> " + referencePage.getFullPath()); 
        } else {
            // failed
            report.setStatus(Report.STATUS_ERROR);
            report.setDescription("ELEMENT ID " + key + " DOESN'T EXIST IN REFERENCE VERSION" + 
                    "\n" + "SOURCE HTML    :> " + sourcePage.getFullPath() + 
                    "\n" + "REFERENCE HTML :> " + referencePage.getFullPath());            
        }           

        return report;        
    }

    /**
     * Compare data from old and new snapshot
     * 
     * @param key of observed object 
     * @param oldPage of html 
     * @param newPage of html
     */
    private Report compareData(String key, HtmlPage oldPage, HtmlPage newPage) {

        Report report = new Report();
        String oldValue = oldPage.getDataByKey(key).getValue();
        String newValue = newPage.getDataByKey(key).getValue();
        String title = oldPage.getDataByKey(key).getTitle();

        if (oldValue.equals(newValue)) {
            // pass
            report.setStatus(Report.STATUS_PASS);
            report.setDescription(title.toUpperCase() + " = " + newValue + 
                    "\n" + "OLD HTML :> " + oldPage.getFullPath() + 
                    "\n" + "NEW HTML :> " + newPage.getFullPath()); 
        } else {
            // failed
            report.setStatus(Report.STATUS_ERROR);
            report.setDescription(
                    title.toUpperCase() + " :> OLD = " + oldValue + 
                    "\n" + title + " :> NEW = " + newValue + 
                    "\n" + "OLD HTML :> " + oldPage.getFullPath() + 
                    "\n" + "NEW HTML :> " + newPage.getFullPath());            
        }           

        return report;
    }

    /**
     * Check file list from both old and new snapshot
     */
    private void checkFileList() {

        Check check = new Check();
        check.setTitle("Test file check list");
        check.setDescription("Check each html files in both old and new snapshot folder. Both snapshot must have similar set of files.");

        for (String oldFile: mOldFileList) {
            Report report = new Report();
            if (mNewFileList.contains(oldFile) == true) {
                // pass                
                report.setDescription(oldFile + " in " + getTapsVersion(mOldPathRoot) + " CONTAINS in " + getTapsVersion(mNewPathRoot));
                report.setStatus(Report.STATUS_PASS);
            } else {
                // failed
                report.setDescription(oldFile + " in " + getTapsVersion(mOldPathRoot) + " DOES NOT CONTAINS in " + getTapsVersion(mNewPathRoot));
                report.setStatus(Report.STATUS_ERROR);
            }
            check.addReport(report);
        }

        for (String newFile: mNewFileList) {
            Report report = new Report();
            if (mOldFileList.contains(newFile) == true) {
                // pass                
                report.setDescription(newFile + " in " + getTapsVersion(mNewPathRoot) + " CONTAINS in " + getTapsVersion(mOldPathRoot));
                report.setStatus(Report.STATUS_PASS);
            } else {
                // failed
                report.setDescription(newFile + " in " + getTapsVersion(mNewPathRoot) + " DOES NOT CONTAINS in " + getTapsVersion(mOldPathRoot));
                report.setStatus(Report.STATUS_ERROR);
            }
            check.addReport(report);
        }        

        mChecks.add(check);
    }

    /**
     * Report all the pass and error scenario. Report should help the user to quickly 
     * find the issue for the regression.
     */
    public void report() {

        int id = 1; 
        int errors = 0;

        for (Check check : mChecks) {

            errors = 0;

            Logger.warning("\n===================================================================================");
            Logger.warning((id++) + ") " + check.getTitle());
            Logger.warning("Description : " + check.getDescription());

            for (Report report : check.getReportList()) {
                if (report.getStatus().equals(Report.STATUS_ERROR)) {                    
                    Logger.warning("\n" + report.getStatus() + " :> \n" + report.getDescription());
                    errors++;
                } else {
                    if (mShowPass == true) 
                        Logger.warning("\n" + report.getStatus() + " :> \n" + report.getDescription());                     
                }
            }

            if (errors == 0) {
                Logger.warning("Status      : Pass\n");
            } else {
                Logger.warning("Status      : Failed\n");
            }
            Logger.warning("\n===================================================================================");

        }   
       
    }

    /**
     * Walk through all files contain in folder and subfolder.
     * 
     * @param folder root folder to walked entirely
     * @param parentFolderPath path of parent
     * @param storeList to store file name
     */
    private void walk(final File folder, String parentFolderPath, ArrayList<String> storeList) {

        String folderName = parentFolderPath + File.separator + folder.getName();

        if (folderName.equals(File.separator + getTapsVersion(mOldPathRoot)) 
                || folderName.equals(File.separator + getTapsVersion(mNewPathRoot))) {
            folderName = "";
        }

        for (final File file : folder.listFiles()) {
            if (file.isDirectory()) {
                walk(file, folderName, storeList);
            } else {
                String fileName = file.getName();
                if (fileName.endsWith(".html")) {
                    Logger.debug("walk", folderName + File.separator + file.getName());
                    storeList.add(folderName + File.separator + file.getName());
                }
            }
        }
    }

    /**
     * return taps version from the root path
     * 
     * @param rootPath of the folder
     */
    private String getTapsVersion(String rootPath) {
        String[] array = rootPath.split("\\" + File.separator);
        return array[array.length-1];
    }

 }