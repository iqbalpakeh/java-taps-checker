/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps;

public class Logger {

    /**
     * Set this to add debug filter 
     */
    private static String mDebugFilter = "xt_st";

    /**
     * Set true to show debug log to user. False otherwise.
     */
    private static boolean mShowDebug = false;

    /**
     * Use this to show message to user
     * 
     * @param message to show to user
     */
    public static void warning(String message) {
        System.out.println(message);
    }

    /**
     * Use this to only show debug message. This function can be deactivate by 
     * setting 
     * 
     * @param tag to select which debug message to show
     * @param message to show to user
     */
    public static void debug(String tag, String message) {
        if (mShowDebug == true && tag.equals(mDebugFilter)) {
            System.out.println("\nDBG " + tag + " => " + message);
        }
    }

}