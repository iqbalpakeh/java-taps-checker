/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps.data;

public class Data {

    private String mId;

    private String mTitle;

    private String mValue;

    public Data() {
        
    }

    public Data(String id, String title, String value) {
        mId = id;
        mValue = value;
        mTitle = title;
    }

    public String getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getValue() {
        return mValue;
    }

}