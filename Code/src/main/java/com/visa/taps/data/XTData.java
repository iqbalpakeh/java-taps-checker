/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps.data;

public class XTData {

    private String mVTF;

    private String mVerdict;

    private String mLab;

    private String mResult;

    private String mCorrect;

    public void setVTF(String vtf) {
        mVTF = vtf;
    }

    public void setVerdict(String verdict) {
        mVerdict = verdict;
    }

    public void setLab(String lab) {
        mLab = lab;
    }

    public void setResult(String result) {
        mResult = result;
    }

    public void setCorrect(String correct) {
        mCorrect = correct;
    }

    public String getVTF() {
        return mVTF;
    }

    public String getVerdict() {
        return mVerdict;
    }

    public String getLab() {
        return mLab;
    }

    public String getResult() {
        return mResult;
    }

    public String getCorrect() {
        return mCorrect;
    }

}