/*********************************************************************
 * Copyright (C) 2018 MPAKEH
 **********************************************************************/

package com.visa.taps;

public class App {
    
    public static void main(String[] args) {
        if (args.length == 3) { 
            Parser parser = new Parser(args[0], args[1], args[2]);
            parser.fetch();
            parser.check();
            parser.report();
        } else {
            Logger.warning("Error!! input has to be three argument ");
            Logger.warning("$ java -jar tapschecker.jar [show or hide] [old_root] [new_root]");
        } 
    }

}
